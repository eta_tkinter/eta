from glob import glob
from importlib import import_module
from imp import reload
from os import path
import os

class moduleHandler():
    def __init__(self,core):
        self.core = core
        self.handler = 'module'

    def listDirectories(self, handler):
        directory = './modules/%s/' % handler
        subdirectories = [ name for name in os.listdir('./modules/%s/' % handler) if path.isdir(path.join(directory, name)) ]
        if '__pycache__' in subdirectories:
            subdirectories.remove('__pycache__')
        return subdirectories

    def listModules(self, handler):
        modules = glob('./modules/%s/*.py' % handler)
        if modules is not None:
            for counter, module in enumerate(modules):
                name = path.split(module)
                modules[counter] = name[1].replace(".py", "")
            if '__init__' in modules:
                modules.remove('__init__')
            return modules
        else:
            return None

    def getModule(self, handler, moduleName):
        try:
            directory = "./modules/%s" % (handler)
            for root, dirs, files in os.walk(directory):
                for name in files:
                    if name == "%s.py" % moduleName:
                        new_path = "{}/{}".format(root, name)
                        new_path = path.relpath(new_path) # remove "./" at beginning
                        new_path = new_path.replace(".py", "") # remove ".py" at end
                        new_path = new_path.replace("/", ".") # change dir/file into dir.file
                        module = import_module(new_path)
                        reload(module)
                        return module.Module()
        except ImportError as e:
            print("""
            Could not load module.
            Possible explanation: you don't have all the packages from the module installed.
            """)
