class profilingHandler():
    def __init__(self,core):
        self.core = core
        self.handler = self.core.handlers["Profiling"]

    def runModule(self,module,data,parameters):
        message = module.run(data, parameters)
        if message is not None:
            print(message)
