import numpy as np
import pandas as pd
import pickle
import os
# from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split
import time

class dataFramesHandler():
	def __init__(self, core):
		self.core = core
		self.dfList = {}

	# Returns a list of dataframes
	def getDataFrames(self):
		return [dataframe for dataframe in self.dfList]

	# Return data from dataframe
	def getData(self,dfName):
		return self.dfList[dfName]

	# Return columns in given dataframe
	def getDataColumns(self,df):
		return self.dfList[df].columns

	# Creates a empty dataframe with the given name
	def createEmptyDataFrame(self,name):
		self.dfList[name] = pd.DataFrame()

	# Get data from the selected dataframe and datacolumns in the GUI
	def getSelectedData(self):
		# Get selected dataframe and datacolumns
		df = self.core.getSelectedDataFrame()
		columns = self.core.getSelectedDataColumns()

		data = pd.DataFrame()
		for column in columns:
			data[column] = self.dfList[df][column]

		return data

	# Add a dataframe
	def addDataFrame(self,name,data):
		self.dfList[name] = data.copy()

	# def copyDataFrame(self, from, to):
	# 	pass

	def convertColumnToDatetime(self, dfName, columnName):
		data = self.dfList[dfName][columnName].ix[:,0]
		print(data)
		# print(data.dtype)
		self.dfList[dfName][columnName] = pd.to_datetime(data)

	# def setIndex(self,dfName,columName):


	def renameDataFrame(self, curName, newName):
		self.dfList[newName] = self.dfList.pop(curName)

	def renameDataColumn(self, dfName, curName, newName):
		self.dfList[dfName].rename(columns = {curName: newName}, inplace=True)

	def removeDataFrame(self, dfName):
		del self.dfList[dfName]

	def removeColumns(self,dfName,columns):
		for column in columns:
			self.removeColumn(dfName,column)

	def removeColumn(self,dfName,column):
		self.dfList[dfName].drop(column, axis=1, inplace=True)

	def resetIndices(self,name):
		try:
			self.dfList[name].reset_index(drop=True)
		except AttributeError as e:
			print(e)

	def addColumn(self,dfName,columnName,data):
		print(data)
		# print(data.ix[:, 0])
		try:
			self.dfList[dfName][columnName] = data.tolist()
			return True
		except ValueError as e:
			self.core.showError('Different size dataframe! Could not add column to %s' % dfName)
			return False

	def splitTrainTest(self, dfName, parameters):
		test_size = float(parameters['test_size'])
		randomized = parameters['randomized']
		testName = 'test_' + dfName
		trainName = 'train_' + dfName
		data = self.getData(dfName)
		if randomized == 'Yes':
			train, test  = train_test_split(data, test_size=test_size)
			self.addDataFrame(testName,test)
			self.addDataFrame(trainName,train)
		else:
			length = len(data)
			split = int(length * (1 - test_size))
			self.addDataFrame(trainName,data[0:split])
			self.addDataFrame(testName,data[split:length])
