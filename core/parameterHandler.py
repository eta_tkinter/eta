import pickle
from os import path

class parameterHandler():
    def __init__(self, core):
        self.core = core

    def getSavedModuleParameters(self, handler, module, case = None):
        if case is None:
            fileName = 'parameters/%s/%s.pkl' % (handler, module.moduleName)
        else:
            fileName = 'parameters/%s/%s-%s.pkl' % (handler, module.moduleName, case)
        if path.isfile(fileName):
            with open(fileName, 'rb') as parametersFile:
                return pickle.load(parametersFile)
        else:
            return self.getModuleDefaults(module, case)

    def getModuleDefaults(self, module, case):
        try:
            if case is None:
                return module.getDefaults()
            else:
                return module.getDefaults(case)
        except Exception as e:
            return None

    def saveModuleParameters(self, handler, module, parameters, case = None):
        if case is None:
            settings = module.getParameters()
        else:
            settings = module.getParameters(case)
        newParameters = {}
        defaults = self.getModuleDefaults(module, case)

        for key, value in settings.items():
            if key in parameters:
                if parameters[key] != '' and parameters[key] != None:
                    if value == 'integer':
                        newParameters[key] = int(float(parameters[key]))
                    elif value == 'float':
                        newParameters[key] = float(parameters[key])
                    else:
                        newParameters[key] = parameters[key]
                else:
                    if key in defaults:
                        newParameters[key] = defaults[key]
                    else:
                        newParameters[key] = None
            else:
                if key in defaults:
                    newParameters[key] = defaults[key]
                else:
                    newParameters[key] = None

        if case is None:
            fileName = 'parameters/%s/%s.pkl' % (handler, module.moduleName)
        else:
            fileName = 'parameters/%s/%s-%s.pkl' % (handler, module.moduleName, case)

        with open(fileName, 'wb') as parametersFile:
            pickle.dump(newParameters, parametersFile)

    def getSavedParameters(self, name):
        fileName = 'parameters/%s.pkl' % (name)
        if path.isfile(fileName):
            with open(fileName, 'rb') as parametersFile:
                return pickle.load(parametersFile)
        else:
            return False

    def saveParameters(self, name, parameters):
        fileName = 'parameters/%s.pkl' % (name)
        with open(fileName, 'wb') as parametersFile:
            pickle.dump(parameters, parametersFile)
