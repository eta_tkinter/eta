# from threading import Thread, Event
import pandas as pd
import numpy as np

import pickle
from os import path

class machineLearningHandler():
    def __init__(self,core):
        self.core = core
        self.handler = self.core.handlers['Machine Learning']

    def fitModule(self, module, data, parameters):
        model = module.fit(data, parameters)
        self.saveModel(module.moduleName, model)

    def loadModel(self, moduleName):
        loadPath = 'models/%s.pkl' % moduleName
        if path.isfile(loadPath):
            with open(loadPath, 'rb') as recentFile:
                model = pickle.load(recentFile)
                return model
        else:
            return False

    def saveModel(self, moduleName, model):
        savePath = 'models/%s.pkl' % moduleName
        with open(savePath, 'wb') as saveFile:
            pickle.dump(model, saveFile)

    def predictModule(self, module, data, parameters):
        model = self.loadModel(module.moduleName)
        newData = module.predict(data, parameters, model)
        columnName = 'predicted'
        self.core.addColumnToCurrentDataFrame(columnName, newData)

    def scoreModule(self, module, data, parameters):
        model = self.loadModel(module.moduleName)
        score = module.score(data, parameters, model)
        print(score)

    def clusterModule(self, module, data, parameters):
        newData = module.run(data, parameters)
        print('D', newData)
        columnName = 'label'
        self.core.addColumnToCurrentDataFrame(columnName, newData)
