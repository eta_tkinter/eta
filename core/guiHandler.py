try:
    import Tkinter as tk # for Python2
    from Tkinter import ttk
    from Tkinter import filedialog as tkfd
    from Tkinter import messagebox as tkmb
    from Tkinter import simpledialog as tksd
except ImportError:
    import tkinter as tk # for Python3
    from tkinter import ttk
    from tkinter import filedialog as tkfd
    from tkinter import messagebox as tkmb
    from tkinter import simpledialog as tksd

from sys import platform

import sys # temp for func name
from os import path
from time import gmtime, strftime # for timestamps in dataframe

import pickle # for exporting and importing parameters

from functools import partial # for creating callback functions

import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')
from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

LARGE_FONT=("Verdana", 12)
NORM_FONT=("Verdana", 10)
SMALL_FONT=("Verdana", 8)

class Menu(ttk.Frame):
    def __init__(self, parent, core, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.core = core

        menubar = tk.Menu(self.parent.root)

        # Check if we're on OS X, replace Python menu with 'ETA'
        if platform == 'darwin':
            from Foundation import NSBundle
            bundle = NSBundle.mainBundle()
            if bundle:
                info = bundle.localizedInfoDictionary() or bundle.infoDictionary()
                if info and info['CFBundleName'] == 'Python':
                    info['CFBundleName'] = 'ETA'

        menuItems = ["File", "Edit", "Plot", "Profiling", "Preprocessing", "Machine Learning", "Help"]
        for menuItem in menuItems:
            menu = tk.Menu(menubar)
            if menuItem == "File":
                menu.add_command(label="Open", command=self.parent.fileOpen)
                menu.add_command(label="Open last", command=self.parent.fileOpenLast)
                menu.add_separator()
                menu.add_command(label="Save as", command=self.parent.fileSaveAs)
                menu.add_separator()
                if platform == 'win32':
                    menu.add_command(label="Quit", command=sys.exit)
                else:
                    menu.add_command(label="Quit", command=quit)
            elif menuItem == "Edit":
                # Selection
                selectMenu = tk.Menu(menu)
                selectMenu.add_command(label="Select none", command=lambda: self.parent.select("none"))
                selectMenu.add_command(label="Select invert", command=lambda: self.parent.select("invert")) # TODO REMOVEME
                selectMenu.add_command(label="Select all", command=lambda: self.parent.select("all"))
                menu.add_cascade(label="Select columns", menu=selectMenu)
                menu.add_separator()
                # Output
                outputMenu = tk.Menu(menu)
                outputMenu.add_command(label="Print selection", command=self.outputSelection)
                menu.add_cascade(label="Print", menu=outputMenu)
                menu.add_separator()
                # Add / rename / remove
                # Add
                menu.add_command(label="Add new dataframe ", command=self.parent.createEmptyDataFrame)
                # Rename
                renameMenu = tk.Menu(menu)
                renameMenu.add_command(label="Rename selected dataframe", command=self.parent.renameDataFrame)
                renameMenu.add_command(label="Rename selected columns", command=self.parent.renameDataColumns)
                menu.add_cascade(label="Rename", menu=renameMenu)
                # Remove
                removeMenu = tk.Menu(menu)
                removeMenu.add_command(label="Remove selected dataframe", command=self.parent.removeDataFrame)
                removeMenu.add_separator()
                removeMenu.add_command(label="Remove selected datacolumns", command=self.parent.removeSelectedColumns)
                removeMenu.add_command(label="Remove unselected datacolumns", command=self.parent.removeUnselectedColumns)
                menu.add_cascade(label="Remove", menu=removeMenu)
                menu.add_separator()
                menu.add_command(label="Convert column to datetime", command=self.parent.convertColumnToDatetime)
            elif menuItem == "Help":
                menu.add_command(label="TODO")
            elif menuItem in self.core.handlers:
                self.addHandlerMenus(menu, menuItem)
            else:
                self.core.showError("Menu item not defined: %s" % menuItem)

            menubar.add_cascade(label=menuItem, menu=menu)

        self.parent.root.config(menu=menubar)

    def addModule(self, menu, handler, moduleName, submenu = None):
        if handler != "Machine Learning":
            menu.add_command(label=moduleName, command=lambda: self.parent.runHandler(handler, moduleName))
        else:
            self.addMachineLearningModule(menu, handler, moduleName, submenu)

    def addMachineLearningModule(self, menu, handler, moduleName, submenu):
        mlMenu = tk.Menu(menu)
        if submenu != 'clustering':
            mlMenu.add_command(label="Fit", command=lambda: self.parent.machineLearning(moduleName, 'fit'))
            mlMenu.add_command(label="Predict", command=lambda: self.parent.machineLearning(moduleName, 'predict'))
            mlMenu.add_command(label="Score", command=lambda: self.parent.machineLearning(moduleName, 'score'))
        else:
            mlMenu.add_command(label="Run", command=lambda: self.parent.machineLearning(moduleName, 'cluster'))
        menu.add_cascade(label=moduleName, menu=mlMenu)

    def addHandlerMenus(self, menu, handler):
        directory = self.core.handlers[handler]

        # Add directories as submenus
        submenus = self.core.listDirectories(directory)
        for submenu in submenus:
            sub = tk.Menu(menu)
            modules = self.core.listModules('{}/{}'.format(directory, submenu))
            for module in modules:
                self.addModule(sub, handler, module, submenu)
            menu.add_cascade(label=submenu, menu=sub)

        # Add modules not in a directory
        modules = self.core.listModules(directory)
        for module in modules:
            self.addModule(menu, handler, module)
        if handler == 'Machine Learning':
            menu.add_command(label="Split dataframe in train and test set", command=self.core.splitTrainTest)

        if submenus is None and modules is None:
            menu.add_command(label="No modules")

    def outputSelection(self):
        df = self.core.getSelectedDataFrame()
        columns = self.core.getSelectedDataColumns()
        data = self.core.getSelectedData()
        text = "DataFrame: {}\nColumns: {}\nData:\n{}""".format(df, columns, data)
        print(text)

class Notebook(ttk.Notebook):
    """A ttk Notebook with close buttons on each tab"""

    __initialized = False

    def __init__(self, *args, **kwargs):
        if not self.__initialized:
            self.__initialize_custom_style()
            self.__inititialized = True

        kwargs["style"] = "CustomNotebook"
        ttk.Notebook.__init__(self, *args, **kwargs)

        self._active = None

        self.bind("<ButtonPress-1>", self.on_close_press, True)
        self.bind("<ButtonRelease-1>", self.on_close_release)

    def on_close_press(self, event):
        """Called when the button is pressed over the close button"""

        element = self.identify(event.x, event.y)

        if "close" in element:
            index = self.index("@%d,%d" % (event.x, event.y))
            self.state(['pressed'])
            self._active = index

    def on_close_release(self, event):
        """Called when the button is released over the close button"""
        if not self.instate(['pressed']):
            return

        element =  self.identify(event.x, event.y)
        index = self.index("@%d,%d" % (event.x, event.y))

        if "close" in element and self._active == index:
            self.forget(index)
            self.event_generate("<<NotebookTabClosed>>")

        self.state(["!pressed"])
        self._active = None

    def __initialize_custom_style(self):
        style = ttk.Style()
        self.images = (
            tk.PhotoImage("img_close", data='''
                R0lGODlhCAAIAMIBAAAAADs7O4+Pj9nZ2Ts7Ozs7Ozs7Ozs7OyH+EUNyZWF0ZWQg
                d2l0aCBHSU1QACH5BAEKAAQALAAAAAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU
                5kEJADs=
                '''),
            tk.PhotoImage("img_closeactive", data='''
                R0lGODlhCAAIAMIEAAAAAP/SAP/bNNnZ2cbGxsbGxsbGxsbGxiH5BAEKAAQALAAA
                AAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU5kEJADs=
                '''),
            tk.PhotoImage("img_closepressed", data='''
                R0lGODlhCAAIAMIEAAAAAOUqKv9mZtnZ2Ts7Ozs7Ozs7Ozs7OyH+EUNyZWF0ZWQg
                d2l0aCBHSU1QACH5BAEKAAQALAAAAAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU
                5kEJADs=
            ''')
        )

        style.element_create("close", "image", "img_close",
                            ("active", "pressed", "!disabled", "img_closepressed"),
                            ("active", "!disabled", "img_closeactive"), border=8, sticky='')
        style.layout("CustomNotebook", [("CustomNotebook.client", {"sticky": "nswe"})])
        style.layout("CustomNotebook.Tab", [
            ("CustomNotebook.tab", {
                "sticky": "nswe",
                "children": [
                    ("CustomNotebook.padding", {
                        "side": "top",
                        "sticky": "nswe",
                        "children": [
                            ("CustomNotebook.focus", {
                                "side": "top",
                                "sticky": "nswe",
                                "children": [
                                    ("CustomNotebook.label", {"side": "left", "sticky": ''}),
                                    ("CustomNotebook.close", {"side": "left", "sticky": ''}),
                                ]
                        })
                    ]
                })
            ]
        })
    ])

class Plot(ttk.Frame):
    def __init__(self, parent, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

    def plot(self, figure):
        if figure:
            self.canvas = FigureCanvasTkAgg(figure, master=self)
            self.canvas.show()
            self.toolbar = NavigationToolbar2TkAgg(self.canvas, self)
            self.toolbar.update()
            self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)
            self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
            plt.close(figure)
        pass

class Dataselection(ttk.Frame):
    def __init__(self, parent, core, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.core = core


        self.datacolumns = DataColumns(self, core)
        self.dataframes = DataFrames(self, core)

        self.dataframes.pack()
        self.datacolumns.pack()

    def getSelectedDataFrame(self):
        index = self.dataframes.dataframes_listbox.curselection()
        # Check if list is not empty
        if len(index) != 0:
            dataframe = self.dataframes.dataframes_listbox.get(index)
            return dataframe
        else:
            return None

    def getSelectedDataColumns(self):
        listbox = self.datacolumns.datacolumns_listbox
        values = [listbox.get(idx) for idx in listbox.curselection()]
        return values

class DataFrames(ttk.Frame):
    def __init__(self, parent, core, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.core = core

        # self.config(highlightbackground="black", bd=10)

        self.dataframes_label = ttk.Label(self, text="DataFrames")

        # Listbox containing the dataframes
        self.dataframes_listbox = tk.Listbox(self, bd=1, selectmode=tk.BROWSE, width=20, exportselection=0)
        self.dataframes_listbox.bind('<<ListboxSelect>>', self.selected)

        self.dataframes_label.pack()
        self.dataframes_listbox.pack()

    def updateDataFramesListbox(self):
        self.dataframes_listbox.delete(0, tk.END)
        dataframes = self.core.getDataFrames()
        if dataframes:
            for name in dataframes:
                self.dataframes_listbox.insert(tk.END, name)
        self.dataframes_listbox.selection_set(tk.ACTIVE)
        self.parent.datacolumns.updateDataColumnsListbox()

    def selected(self, event):
        self.dataframes_listbox.activate(self.dataframes_listbox.curselection())
        self.parent.datacolumns.updateDataColumnsListbox()

class DataColumns(ttk.Frame):
    def __init__(self, parent, core, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.core = core

        self.datacolumn_frame = ttk.Frame(self)
        self.datacolumns_label = ttk.Label(self.datacolumn_frame, text="DataColumns")

        self.datacolumns_listbox = tk.Listbox(self.datacolumn_frame, bd=1, selectmode=tk.MULTIPLE, width=20, exportselection=0)
        self.datacolumns_listbox.bind('<<ListboxSelect>>', self.selected)

        self.datacolumns_label.pack()
        self.datacolumns_listbox.pack()

        self.datacolumn_frame.pack()

    def updateDataColumnsListbox(self):
        # Clear the listbox
        self.datacolumns_listbox.delete(0, tk.END)

        df = self.parent.getSelectedDataFrame()
        if df:
            datacolumns = self.core.getDataColumns(df)
            if datacolumns is not None:
                for column in datacolumns:
                    self.datacolumns_listbox.insert(tk.END, column)
                self.datacolumns_listbox.selection_set(0, tk.END)

    def selected(self, event):
        pass

    def selectAll(self):
        self.datacolumns_listbox.selection_set(0, tk.END)

    def selectInvert(self):
        self.core.showError("TODO: selectInvert") # TODO

    def selectNone(self):
        self.datacolumns_listbox.selection_clear(0, tk.END)

class Body(ttk.Frame):
    def __init__(self, parent, core, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.core = core

        for r in range(3):
            self.columnconfigure(r, weight=1)
            self.rowconfigure(r, weight=1)

        self.dataselection = Dataselection(self, core)
        self.notebook = Notebook(self)
        self.notebook.enable_traversal() # TODO DOCUMENT
        self.plot = []
        self.plotNumber = 1

        self.dataselection.grid(column=0, rowspan=2, row=0, sticky="NW", pady=10, padx=10)
        self.notebook.grid(column=1, row=0, rowspan=2)

class ParameterWindow(tksd.Dialog):

    def body(self, master):
        self.success = False
        self.parameters = self.parent.parameters
        self.currentParameters = self.parent.currentParameters
        self.inputs = {}
        i = 0
        for parameter in self.parameters:
            parameterType = self.parameters[parameter]
            if self.currentParameters is not None:
                if parameter in self.currentParameters:
                    current = self.currentParameters[parameter]
                else:
                    current = None
            else:
                current = None

            ttk.Label(master, text=parameter).grid(row=i, column=0, sticky="E")

            if parameterType == 'integer' or parameterType == 'float' or parameterType == 'string' :
                entry = ttk.Entry(master)
                if current is not None:
                    entry.insert(0,current)
            elif parameterType == 'boolean':
                var = tk.IntVar()
                entry = tk.Checkbutton(master, variable=var, onvalue=True, offvalue=False)
                entry.var = var
                if current:
                    entry.select()
                else:
                    entry.deselect()
            elif parameterType == 'columns' or parameterType == 'column':
                columns = list(self.parent.core.dataframes.getSelectedData().columns)
                entry = ttk.Combobox(master, values=columns, state='readonly')
                if current is not None and current in columns:
                    entry.set(current)
            elif isinstance(parameterType, list): #if list
                entry = ttk.Combobox(master, values=parameterType, state='readonly')
                if current is not None:
                    entry.set(current)
            else:
                print('ParameterType {} is not a valid type.'.format(parameterType))
                entry = None

            self.inputs[parameter] = entry
            self.inputs[parameter].grid(row=i, column=1, sticky="W")

            if i == 0:
                initial = parameter
            i += 1

        return self.inputs[initial] # initial focus

    def validate(self):
        self.new_parameters = {}
        try:
            for parameter in self.inputs:
                param_type = self.parameters[parameter]
                if param_type == 'integer':
                    new_param = int(self.inputs[parameter].get())
                elif param_type == 'float':
                    new_param = float(self.inputs[parameter].get())
                elif param_type == 'string':
                    new_param = self.inputs[parameter].get()
                elif param_type == 'boolean':
                    new_param = self.inputs[parameter].var.get()
                else:
                    new_param = self.inputs[parameter].get()
                self.new_parameters[parameter] = new_param
            return 1
        except ValueError:
            tkmb.showwarning(
                "Bad input",
                "Illegal values, please try again"
            )
            self.success = False
            return 0

    def apply(self):
        self.success = True

class MainWindow(ttk.Frame):
    def __init__(self, parent, core, *args, **kwargs):
        ttk.Frame.__init__(self, parent, *args, **kwargs)
        self.core = core
        self.root = parent

        menubar = Menu(self, core)
        self.body = Body(self, core)

        self.body.pack(side=tk.TOP, fill="both", expand=True)

        button = ttk.Button(self, text="Quit", command=self.root.quit)
        button.pack(side=tk.BOTTOM, fill=tk.X, expand=True)

    # File menu functions
    def fileOpen(self):
        """Returns an opened file in read mode."""

        self.file_opt = options = {}
        # options['message'] = "Choose a data file to load"
        options['parent'] = self.root
        # options['title'] = 'This is a title'

        filename = tkfd.askopenfilename(**self.file_opt)

        if filename: # check if not cancelled
            if path.isfile(filename): # check if file exists
                if self.core.loadFile(filename):
                    # Save file as last opened
                    savePath = 'tmp/recent.pkl'
                    with open(savePath, 'wb') as saveFile:
                        pickle.dump(filename, saveFile)
                else:
                    self.core.showError("Could not load file")
            else:
                self.core.showError("File doesn't exist","File doesn't exist")

    def fileOpenLast(self):
        loadPath = 'tmp/recent.pkl'
        if path.isfile(loadPath):
            with open(loadPath, 'rb') as recentFile:
                fileName = pickle.load(recentFile)
                if self.core.loadFile(fileName):
                    return
                else:
                    print("Could not load last opened file: %s" % fileName)
        else:
            self.core.showError("No recent file discovered.","No recent file discovered.")

    def fileSaveAs(self):
        df = self.body.dataselection.getSelectedDataFrame()
        data = self.core.dataframes.getSelectedData()

        fileName, fileExtension = path.splitext(df)

        self.file_opt = options = {}
        options['parent'] = self.root
        options['defaultextension'] = '.pickle'
        options['filetypes'] = [("Python's Pickle",'*.pickle'),('CSV - Comma Seperated Values','*.csv'),('XLSX - Excel','*.xlsx')]
        options['initialfile'] = fileName + '_saved'
        options['message'] = 'Save selected data' # macOS title
        options['title'] = 'Save selected data'

        filename = tkfd.asksaveasfilename(**options)
        if filename is not None and filename is not '':
            self.core.files.saveData(data, filename)

    # Edit menu functions
    def select(self, which):
        if which == 'all':
            self.body.dataselection.datacolumns.selectAll()
        elif which == 'invert':
            pass
        elif which == 'none':
            self.body.dataselection.datacolumns.selectNone()
        else:
            self.core.showError("Selection not possible: %s" % which, "Selection not possible")

    def removeSelectedColumns(self):
        df = self.getSelectedDataFrame()
        columns = self.getSelectedDataColumns()
        self.core.removeColumns(df, columns)

    def removeUnselectedColumns(self):
        df = self.getSelectedDataFrame()
        selection_columns = self.getSelectedDataColumns()
        all_columns = self.core.getDataColumns(df)
        columns = [column for column in all_columns if column not in selection_columns]
        self.core.removeColumns(df, columns)

    def createEmptyDataFrame(self):
        self.core.createEmptyDataFrame()

    def renameDataFrame(self):
        dfName = self.getSelectedDataFrame()
        name = tksd.askstring("Change name of {}".format(dfName), "New name")
        if name is not None:
            self.core.renameDataFrame(dfName, name)

    def renameDataColumns(self):
        dfName = self.getSelectedDataFrame()
        dfColumns = self.getSelectedDataColumns()
        for column in dfColumns:
            name = tksd.askstring("Change name of {}".format(column), "New name")
            if name is not None:
                self.core.renameDataColumn(dfName, column, name)

    def removeDataFrame(self):
        dfName = self.getSelectedDataFrame()
        self.core.removeDataFrame(dfName)

    def convertColumnToDatetime(self):
        dfName = self.getSelectedDataFrame()
        dfColumns = self.getSelectedDataColumns()
        if len(dfColumns) == 1:
            self.core.convertColumnToDatetime(dfName, dfColumns)
        else:
            self.core.showError("Too many columns selected. Only one column should be selected.")


    def getSelectedDataFrame(self):
        return self.body.dataselection.getSelectedDataFrame()

    def getSelectedDataColumns(self):
        return self.body.dataselection.getSelectedDataColumns()

    # Handler menu functions
    def runHandler(self, handler, moduleName):
        if handler == 'Plot':
            self.plot(moduleName)
        elif handler == 'Profiling':
            self.profile(moduleName)
        elif handler == 'Preprocessing':
            self.preprocess(moduleName)
        elif handler == 'Machine Learning':
            self.machineLearning(moduleName)
        else:
            self.core.showError('Handler does not exist')

    def plot(self, moduleName):
        figure = self.core.runPlot(moduleName)
        if figure:
            plotTab = Plot(self.body.notebook)
            self.body.plot.append(plotTab)
            self.body.notebook.add(plotTab, text='Plot {}'.format(self.body.plotNumber))
            self.body.plotNumber += 1
            self.body.notebook.select(plotTab)
            plotTab.plot(figure)

    def profile(self, moduleName):
        self.core.runProfiling(moduleName)

    def preprocess(self, moduleName):
        self.core.runPreprocess(moduleName)

    def machineLearning(self, moduleName, case):
        if case == 'fit':
            self.core.runMachineLearningFit(moduleName)
        elif case == 'predict':
            self.core.runMachineLearningPredict(moduleName)
        elif case == 'score':
            self.core.runMachineLearningScore(moduleName)
        elif case == 'cluster':
            self.core.runMachineLearningCluster(moduleName)
        else:
            self.core.showError("Cannot perform %s in machine learning" % case)

    # General functions
    def createParameterWindow(self, parameters, currentParameters):
        self.parameters = parameters
        self.currentParameters = currentParameters
        self.parameterWindow = ParameterWindow(self, "Set Parameters")
        if self.parameterWindow.success:
            return self.parameterWindow.new_parameters
        else:
            return False

    def updateDataView(self):
        self.body.dataselection.dataframes.updateDataFramesListbox()

class guiHandler:
    def __init__(self, core):
        self.core = core
        self.root = tk.Tk()
        self.root.wm_title("ETA")
        self.root.geometry("1080x720+90+90")
        # self.root.geometry("1440x900")
        self.window = MainWindow(self.root, self.core)
        self.window.pack(side="top", fill="both", expand=True)
        self.root.lift()

    def run(self):
        self.root.mainloop()

    def showErrorMessage(self, message, title='Error'):
        tkmb.showerror(title, message, icon=tkmb.ERROR)

    def askContinue(self, message):
        return tkmb.askokcancel("Continue?", message)
