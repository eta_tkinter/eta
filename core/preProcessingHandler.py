from threading import Thread
import pandas as pd
import numpy as np

from time import gmtime, strftime

class preProcessingHandler():
    def __init__(self,core):
        self.core = core
        self.handler = 'preprocessing'

    def preProcess(self,parameters=None):
        t = Thread(target=self.runModule, args=(parameters,))
        t.start()

    def runModule(self, module, data, parameters):
        # Run the module
        returned = module.run(data,parameters)

        # Check the returned data
        if type(returned) != type(dict()):
            newData = returned # returned contains the new data

            # Create a new data frame only when data length differs
            if len(data) == len(newData):
                newDataFrame = False
            else:
                newDataFrame = True

            name_addition = module.moduleName
        else:
            if 'data' in returned:
                newData = returned['data']
            else:
                self.core.showError('No data returned from module.')
                return

            if 'newDataFrame' in returned:
                newDataFrame = returned['newDataFrame']
            else:
                # Create a new data frame only when data length differs
                if len(data) == len(newData):
                    newDataFrame = False
                else:
                    newDataFrame = True

            if 'name_addition' in returned:
                name_addition = returned['name_addition']
            else:
                name_addition = module.moduleName

        # Add new data
        if newData is not None:
            if newDataFrame is True:
                dfName = 'generated_'+module.moduleName+'_'+str(strftime("%Y%m%d_%H%M%S", gmtime()))
                self.core.dataframes.addDataFrame(dfName,newData)
            else:
                for column in newData:
                    columnName = "{}_{}".format(column, name_addition)
                    dfName = parameters['dfName']
                    if not self.core.dataframes.addColumn(dfName,columnName,newData[column]):
                        return

            self.core.updateDataView()
            return
        else:
            self.core.showError("Returned data is none.")
            return
