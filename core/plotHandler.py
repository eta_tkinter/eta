import matplotlib
matplotlib.use('TkAgg')

class plotHandler():
    def __init__(self,core):
        self.core = core
        self.handler = 'plot'

    def runModule(self,module,data,parameters):
        if (data is not None):
            return module.run(data,parameters)
        else:
            print("No data loaded yet")
        return

    # REVIEW
    # def plotCorrelationMatrix(self,data):
    #     axes = pd.tools.plotting.scatter_matrix(data, alpha=0.2)
    #     plt.tight_layout()
    #     plt.savefig('scatter_matrix.png')
    #     return plot
    #     pass
