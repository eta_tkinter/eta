from .moduleHandler import *
from .parameterHandler import *
from .filesHandler import *
from .dataframesHandler import *
from .plotHandler import *
from .profilingHandler import *
from .preProcessingHandler import *
from .machineLearningHandler import *
from .guiHandler import *

class coreHandler:
    def __init__(self):

        # Define handler names and their camelcase name
        self.handlers = {
            "Profiling": "profiling",
            "Plot": "plot",
            "Preprocessing": "preprocessing",
            "Machine Learning": "machineLearning"
        }

        # Define handler objects
        self.modules = moduleHandler(self)
        self.parameters = parameterHandler(self)
        self.files = filesHandler(self)
        self.dataframes = dataFramesHandler(self)
        self.plot = plotHandler(self)
        self.profiling = profilingHandler(self)
        self.preprocess = preProcessingHandler(self)
        self.machineLearning = machineLearningHandler(self)
        self.gui = guiHandler(self)

        # self.gui.window.fileOpenLast() # TODO REMOVEME

        # Run tkinter mainloop
        self.gui.run()

    # GUI FUNCTIONS
    def askContinue(self, message):
        return self.gui.showContinue(message)

    def showError(self, message, title='Error'):
        self.gui.showErrorMessage(message, title)

    def updateDataView(self):
        self.gui.window.updateDataView()

    def getSelectedDataFrame(self):
        return self.gui.window.getSelectedDataFrame()

    def getSelectedDataColumns(self):
        return self.gui.window.getSelectedDataColumns()

    # MODULES FUNCTIONS
    def listDirectories(self, handler):
        return self.modules.listDirectories(handler)

    def listModules(self, handler):
        return self.modules.listModules(handler)

    # DATAFRAMES FUNCTIONS
    def getData(self, df):
        return self.dataframes.getData(df)

    def getDataColumns(self, df):
        return self.dataframes.getDataColumns(df)

    def getSelectedData(self):
        return self.dataframes.getSelectedData()

    def getDataFrames(self):
        return self.dataframes.getDataFrames()

    def createEmptyDataFrame(self):
        name = 'new_{}'.format(str(strftime("%y%m%d_%H%M%S", gmtime())))
        self.dataframes.createEmptyDataFrame(name)
        self.updateDataView()

    def addColumnToCurrentDataFrame(self, columnName, data):
        dfName = self.getSelectedDataFrame()
        self.dataframes.addColumn(dfName, columnName, data)
        self.updateDataView()

    def convertColumnToDatetime(self, dfName, columnName):
        self.dataframes.convertColumnToDatetime(dfName, columnName)

    def renameDataFrame(self, curName, newName):
        self.dataframes.renameDataFrame(curName, newName)
        self.updateDataView()

    def renameDataColumn(self, dfName, curName, newName):
        self.dataframes.renameDataColumn(dfName, curName, newName)
        self.updateDataView()

    def removeColumns(self, dfName, columns):
        self.dataframes.removeColumns(dfName, columns)
        self.updateDataView()

    def removeDataFrame(self, dfName):
        self.dataframes.removeDataFrame(dfName)
        self.updateDataView()

    def splitTrainTest(self):
        name = 'splitTrainTest'
        dfName = self.getSelectedDataFrame()
        parameters = {
            'test_size': 'float',
            'randomized': ['Yes', 'No']
        }
        defaults = {
            'test_size': '0.33',
            'randomized': 'Yes'
        }
        savedParameters = self.parameters.getSavedParameters(name)
        if not savedParameters:
            savedParameters = defaults
        newParameters = self.askParameters(parameters, savedParameters)
        if newParameters:
            self.parameters.saveParameters(name, newParameters)
            self.dataframes.splitTrainTest(dfName, newParameters)
            self.updateDataView()

    # FILES FUNCTIONS
    def loadFile(self, filename):
        df = self.files.loadFile(filename)
        if df is not False:
            dfName = path.basename(filename)
            self.dataframes.addDataFrame(dfName,df)
            self.dataframes.resetIndices(dfName)
            self.updateDataView()
            return True
        else:
            return False

    # MIXED FUNCTIONS
    def askParameters(self, parameters, defaults):
        return self.gui.window.createParameterWindow(parameters, defaults)

    def runParameters(self, handler, module, case = None):
        try:
            if case is None:
                parameters = module.getParameters()
            else:
                parameters = module.getParameters(case)
        except Exception as e:
            # self.showError('Could not get parameters')
            return None # no getParameters()
            # return False REVIEW

        if parameters:
            currentParameters = self.parameters.getSavedModuleParameters(handler, module, case)
            newParameters = self.askParameters(parameters, currentParameters)
        else:
            return None # Module doesn't have parameters

        if newParameters is not False: # Not Cancelled
            self.parameters.saveModuleParameters(handler, module, newParameters, case)
            return newParameters
        else:
            return False

    def runPlot(self, moduleName):
        handler = self.plot.handler
        module = self.modules.getModule(handler, moduleName)
        data = self.dataframes.getSelectedData()
        parameters = self.runParameters(handler, module)
        if parameters is not False:
            return self.plot.runModule(module, data, parameters)
        else: # Cancelled
            return False

    def runProfiling(self, moduleName):
        handler = self.profiling.handler
        module = self.modules.getModule(handler, moduleName)
        data = self.dataframes.getSelectedData()
        parameters = self.runParameters(handler, module)
        if parameters is not False:
            self.profiling.runModule(module, data, parameters)
            # self.updateDataView() # For new dataframes

    def runPreprocess(self, moduleName):
        handler = self.preprocess.handler
        module = self.modules.getModule(handler, moduleName)
        data = self.dataframes.getSelectedData()
        parameters = self.runParameters(handler, module)
        if parameters is not False:
            # Add dfName for creation of new data frame
            if parameters is None:
                parameters = {}
            parameters['dfName'] = self.gui.window.getSelectedDataFrame() # REVIEW
            self.preprocess.runModule(module, data, parameters)
            self.updateDataView() # For new dataframes

    def runMachineLearningFit(self, moduleName):
        case = 'fit'
        handler = self.machineLearning.handler
        module = self.modules.getModule(handler, moduleName)
        data = self.dataframes.getSelectedData()
        parameters = self.runParameters(handler, module, case)
        if parameters is not False:
            self.machineLearning.fitModule(module, data, parameters)

    def runMachineLearningPredict(self, moduleName):
        case = 'predict'
        handler = self.machineLearning.handler
        module = self.modules.getModule(handler, moduleName)
        data = self.dataframes.getSelectedData()
        parameters = self.runParameters(handler, module, case)
        if parameters is not False:
            self.machineLearning.predictModule(module, data, parameters)

    def runMachineLearningScore(self, moduleName):
        case = 'score'
        handler = self.machineLearning.handler
        module = self.modules.getModule(handler, moduleName)
        data = self.dataframes.getSelectedData()
        parameters = self.runParameters(handler, module, case)
        if parameters is not False:
            self.machineLearning.scoreModule(module, data, parameters)
        self.updateDataView()

    def runMachineLearningCluster(self, moduleName):
        case = None
        handler = self.machineLearning.handler
        module = self.modules.getModule(handler, moduleName)
        data = self.dataframes.getSelectedData()
        parameters = self.runParameters(handler, module, case)
        if parameters is not False:
            self.machineLearning.clusterModule(module, data, parameters)
