import numpy as np
import pandas as pd
import pickle
# from os import path
from os import path
import time
import xlsxwriter # to save to xlsx

class filesHandler():
	def __init__(self, core):
		self.core = core

	def loadFile(self,filepath):
		if not filepath:
			return False
		else:
			fileName, fileExtension = path.splitext(filepath)
			if fileExtension == '.pickle' or fileExtension == '.pkl':
				return pd.read_pickle(filepath)
			elif fileExtension == '.csv':
				# http://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html
				return pd.read_csv(filepath,engine='python',parse_dates=True,infer_datetime_format=True)
			elif fileExtension == '.xlsx':
				return pd.read_excel(open(filepath,'rb'), sheetname=0)
			else:
				try:
					return pd.read_table(filepath,sep=None,engine='python')
				except:
					self.core.showError("Could not read file: %s" % filepath, "Could not read file")
					return False
		return False

	def checkMissingData(self):
		percentage = ((len(self.data.index)-(len(self.data.index)-self.data.count()))/len(self.data.index))*100
		return percentage

	def saveData(self,data,fileName):
		tmp, fileExtension = path.splitext(path.basename(fileName)) # get fileExtension

		if fileExtension == '.pickle' or fileExtension == '.pkl':
			fileStream = open(fileName,'wb') # open as write bytes
			pickle.dump(data,fileStream)
		elif fileExtension == '.csv':
			fileStream = open(fileName,'w') # open as write string
			data.to_csv(fileStream,index=False)
		elif fileExtension == '.xlsx':
			fileStream = open(fileName,'wb') # open as write bytes
			writer = pd.ExcelWriter(fileStream,engine='xlsxwriter')
			data.to_excel(writer)
			writer.save()
		else:
			self.core.showError("File extension differs from .pickle/.pkl, .csv, or .xlsx")
			return

		fileStream.close()

	def saveModel(self,model,filename):
		fileStream = open(filename,'wb')
		# this writes the object a to the
		pickle.dump(model,fileStream)
		# here we close the fileObject
		fileStream.close()

	def loadModel(self,filename):
		fileStream = open(filename,'rb')
		model = pickle.load(fileStream)
		fileStream.close()

		return model
