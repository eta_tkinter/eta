import numpy as np
import pandas as pd
from sklearn.preprocessing import Imputer
class preProcess_Module():
	def __init__(self):
		pass
	def getOptions(self):
		#key => value
		#value is list => dropdown
		#value is str(float) => empty field
		settings = {
			#'missing_values':'float',
			'strategy':['mean','median','most_frequent'],
			#'amount':'float',
			#'column': 'columns'
		}
		return settings
	def run(self,dataFrames,parameters=None):
		imp = Imputer(missing_values='NaN', strategy=parameters['strategy'] )

		maskedData = dataFrames['maskedData']
		#filesHandlerObject.maskedData
		return imp.fit(maskedData)
