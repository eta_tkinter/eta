import numpy as np
import pandas as pd

from collections import OrderedDict # for keeping options in order

from sklearn.preprocessing import Imputer

class Module():
	def __init__(self):
		self.moduleName = 'replace_missing'

	def getParameters(self):
		settings = OrderedDict([
			('newDataFrame', 'boolean'),
			('strategy', ['mean','median','most_frequent']),
		])
		return settings

	def getDefaults(self):
		defaults = {
			'newDataFrame': False,
			'strategy': 'mean',
		}
		return defaults

	def run(self,data,parameters=None):
		# newDataFrame = parameters['newDataFrame']
		strategy = parameters['strategy']

		imp = Imputer(missing_values='NaN', strategy=strategy, copy=True )

		X = imp.fit_transform(data)
		newData = pd.DataFrame(data=X,columns=data.columns.values)

		returned = {
			'newDataFrame': False,
			'data': newData,
			'name_addition': self.moduleName
		}

		return returned
