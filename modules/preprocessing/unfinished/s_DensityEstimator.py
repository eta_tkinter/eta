import numpy as np
import pandas as pd
import math
from sklearn.neighbors.kde import KernelDensity

class Module():
	def __init__(self):
		self.moduleName = 's_density_estimator'

	def getParameters(self):
		settings = {
			'algorithm': ['kd_tree','ball_tree','auto'],
			'kernel':['gaussian','tophat','epanechnikov','exponential','linear']
		}
		return settings

	def getDefaults(self):
		settings = {
			'algorithm' : 'auto',
			'kernel':'gaussian'
		}
		return settings

	def run(self,dataFrames,parameters=None):
		maskedData = dataFrames
		newData = pd.DataFrame()

		for column in maskedData:
			kde = KernelDensity(algorithm=parameters['algorithm'],kernel=parameters['kernel']).fit(maskedData[column].as_matrix().reshape(-1, 1))

			newData[column] = kde.score_samples(maskedData[column].as_matrix().reshape(-1, 1))
		return newData
