from __future__ import division # bugfix

import numpy as np
import pandas as pd

from collections import OrderedDict # for keeping options in order

class Module():
	def __init__(self):
		self.moduleName = 'arithmetics'

	def getParameters(self):
		settings = OrderedDict([
			('timestamp_field', 'maskedColumns'),
			('aggregation', ['anomalies','average','cumsum','mean','median','std','sum','var','max','min']),
			('create_dataframe', ['Yes','No']),
		])
		return settings

	def getDefaults(self):
		defaults = {
			'timestamp_field': 'time',
			'aggregation': 'mean',
			'create_dataframe': 'No',
		}
		return defaults

	def run(self, data, parameters):
		timestamp_field = parameters['timestamp_field']
		aggregation = parameters['aggregation']
		create_dataframe = parameters['create_dataframe']

		if create_dataframe == 'Yes':
			newDataFrame = True
		else:
			newDataFrame = False

		newData = data.drop([timestamp_field], axis=1)
		# newData = newData.reset_index(drop=True)
		newData = newData.apply(np.sum, axis=1)
		print(newData)
		# if newDataFrame == True:
		# 	newData[timestamp_field] = newData.index.values
		# newData = newData.reset_index(drop=True)

		returned = {
			'newDataFrame': newDataFrame,
			'data': newData,
			'name_addition': aggregation
		}

		return returned
