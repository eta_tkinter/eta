import numpy as np
import pandas as pd
class preProcess_Module():
	def __init__(self):
		pass
	def getName(self):
		return 'Znormalize'
	def getOptions(self):
		#key => value
		#value is list => dropdown
		#value is str(float) => empty field

		return None
	def run(self,dataFrames,parameters=None,):
		maskedData = dataFrames['maskedData']
		newMaskedData = pd.DataFrame()
		for column in maskedData:
			newMaskedData[column] = (maskedData[column] - maskedData[column].mean())/maskedData[column].std(ddof=0)

		return newMaskedData
		# RETURN DATA, MESSAGE , IF WHOLE DATASET NEEDS TO BE REPLACED
