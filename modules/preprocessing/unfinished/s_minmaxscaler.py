import numpy as np
import pandas as pd
from sklearn import preprocessing
class preProcess_Module():
	def __init__(self):
		pass
	def getOptions(self):
		#key => value
		#value is list => dropdown
		#value is str(float) => empty field

		return None
	def run(self,dataFrames,parameters=None,):
		newMaskedData = pd.DataFrame()
		maskedData = dataFrames['maskedData']
		for column in maskedData:
			col_minmaxscaler = column + '_minmax_scaler'
			min_max_scaler = preprocessing.MinMaxScaler()
			newMaskedData[col_minmaxscaler] = min_max_scaler.fit_transform(maskedData[column].values)
		return newMaskedData,
		# RETURN DATA, MESSAGE , IF WHOLE DATASET NEEDS TO BE REPLACED
