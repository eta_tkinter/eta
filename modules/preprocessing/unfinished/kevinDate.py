import numpy as np
import pandas as pd

class Module():
    def __init__(self):
        self.moduleName = 'kevinDate'

    def getParameters(self):
        from collections import OrderedDict # for keeping options in order
        settings = OrderedDict([
            ('column', 'columns')
        ])
        return settings

    def run(self, data, parameters):
        column = parameters['column']
        data[column] = pd.to_datetime(data[column])
        newData = pd.DataFrame()
        newData[column + ' year'] = data[column].dt.year
        newData[column + ' month'] = data[column].dt.month
        newData[column + ' day'] = data[column].dt.day
        return newData
