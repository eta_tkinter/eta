import numpy as np
import pandas as pd
import math
from sklearn.neighbors.kde import KernelDensity

import logging
import argparse

import numpy as np
from scipy.cluster.vq import kmeans2
from operator import itemgetter

import os.path

import heapq
from collections import defaultdict
from numpy import log2

import cPickle

class Module():
	def __init__(self):
		self.moduleName = "vespier_motif_discovery"

	def getDefaults(self):
		settings = {
			'alphabet_size': '5',
			'scale_quantization':'power2',
			'min_support':'2',
			'subsampling_factor':'1',
			'pattern_set_size':'10',
		}
		return settings

	def getParameters(self):
		settings = {
			'alphabet_size': 'integer',
			'scale_quantization':['power2'],
			'min_support':'integer',
			'subsampling_factor':'integer',
			'pattern_set_size':'integer',
		}
		return settings

	def run(self,data,parameters=None):
		newData = pd.DataFrame()
		main(data,parameters)
		return newData


class HuffmanTreeNode:
	''' Main Huffman tree node. It is a leaf if left==right==None and
		symbol != None, otherwise it is an internal node. '''

	def __init__(self, frequency, symbol=None, childLeft=None, childRight=None):
		self.frequency = frequency
		self.left = childLeft
		self.right = childRight
		self.symbol = symbol

	def __cmp__(self, other):
		return cmp(self.frequency, other.frequency)

def build_scale_space(data, scales, order=0):
	sSpace = data.copy()
	for s in scales:
		component, kernel = gaussian_convolution(data, s, order=order)
		sSpace = np.vstack((sSpace, component))
	return sSpace

def discretize_equal_width(x, bins=256, min=None, max=None, input_domain=True):
	''' Discretize the time series t according to an alphabet of bins symbols. The bins are computed to have equal width. '''

	if min is None and max is None:
		min = np.min(x)
		max = np.max(x)

	step = (max - min) / (bins - 1)
	intervals = np.linspace(min, max + 0.000001, num=bins + 1)

	if input_domain:
		return (np.digitize(x, intervals) - 1) * step + min
	else:
		return np.digitize(x, intervals) - 1



def huffmanEncodedLength(t):
	''' Computes the number of bits of t after Huffman encoding. '''

	frequencies = computeFrequencies(t)
	huffmanTree = computeHuffmanTree(frequencies)
	codeTable = defaultdict(int)
	computeHuffmanCodeTable(huffmanTree, codeTable)

	length = 0
	for x in t:
		length += codeTable[x]
	return length

def gaussian_convolution(signal, sigma, cutoff=3.0, order=0, boundaryMode='same'):
	# snippet adapted from scipy.ndimage
	kw = int(cutoff * sigma + 0.5)
	kernel = np.zeros(2 * kw + 1, dtype='float32')
	kernel[kw] = 1.0
	sum = 1.0
	sigma = sigma * sigma
	# calculate the kernel:
	for i in xrange(1, kw + 1):
		tmp = np.exp(-0.5 * float(i * i) / sigma)
		kernel[kw + i] = tmp
		kernel[kw - i] = tmp
		sum += 2.0 * tmp
	for i in xrange(2 * kw + 1):
		kernel[i] /= sum
	# implement first and second derivatives:
	if order == 1:  # first derivative
		kernel[kw] = 0.0
		for i in range(1, kw + 1):
			x = float(i)
			tmp = -x / sigma * kernel[kw + i]
			kernel[kw + i] = -tmp
			kernel[kw - i] = tmp
	elif order == 2:  # second derivative
		kernel[kw] *= -1.0 / sigma
		for i in range(1, kw + 1):
			x = float(i)
			tmp = (x * x / sigma - 1.0) * kernel[kw + i] / sigma
			kernel[kw + i] = tmp
			kernel[kw - i] = tmp
	# Mode same returns output of length max(signal, kernel). Boundary effects are visible.

	return np.convolve(signal, kernel, mode=boundaryMode), kernel

def _zero_crossings(x):
	out = np.diff(np.sign(np.diff(x))).nonzero()[0] + 1
	out = np.insert(out, 0, 0)
	out = np.append(out, len(x)-1)
	return out


def _z_normalize(s):
		return (s - s.mean()) / s.std()


def get_ngrams(strg, n, min_count=2):
	def _to_string(x):
		s = ''
		for v in x:
			s = s + '%d#' % v
		return s

	def _is_overlapping(pos, z):
		# TODO
		return False

	counter = dict()
	for i in xrange(len(strg) - n + 1):
		gram = strg[i:i + n]
		if _to_string(gram) in counter:
			if i - counter[_to_string(gram)][-1] > n - 2:
				counter[_to_string(gram)].append(i)
		else:
			counter[_to_string(gram)] = [i]
	ngrams = [(len(pos), pos, n, [int(x) for x in gram.split('#')[:-1]]) for gram, pos in counter.iteritems() if len(pos) >= min_count and not _is_overlapping(pos, None)]

	return ngrams


def candidate_search(d, real, k, min_p=2, max_p=10, min_support=2, counter=0):

	z = _zero_crossings(d)
	dx = np.diff(z)
	dy = np.diff(d[z])
	z = z[1:]

	dx = _z_normalize(dx)
	dy = _z_normalize(dy)
	pspace = np.array(zip(dx, dy))

	#plot_pspace(dx, dy, counter)

	logging.info('Features quantization.')
	centroids, clusters = kmeans2(pspace, k)

	#plot_features(d, z, clusters, counter)

	logging.info('Finding reoccurring events.')
	patterns = []
	for i in xrange(min_p, max_p + 1):
		patterns += get_ngrams(clusters.tolist(), i, min_support)

	# Computing coverage and ranking patterns
	for i, pattern in enumerate(patterns):

		coverage = 0.0
		lengths = []
		for p in pattern[1]:
			coverage += z[p + pattern[2] - 1] - z[p - 1]
			lengths.append(z[p + pattern[2] - 1] - z[p - 1])

		#mean_length = coverage / float(len(pattern[1]))

		coverage /= len(d)
		patterns[i] = (pattern[0], np.array(pattern[1]), pattern[2], coverage, np.array(lengths), pattern[-1])

	patterns = sorted(patterns, key=itemgetter(3), reverse=True)

	return patterns, z


def apply_pattern_model(approx, mask, pattern, d, z):
	for start in pattern[1]:
		for i in range(pattern[2]):
			event_length = z[start+i] - z[start+i-1]
			slope = (d[z[start+i]] - d[z[start+i-1]])/event_length
			approx[z[start+i-1]:z[start+i]] = np.arange(event_length)*slope + d[z[start+i-1]]
			mask[z[start+i-1]:z[start+i]] = 0
	return approx


def cover_cost(cover, sspace):
	cost = 0
	x_diffs = []
	y_diffs = []
	for scale_index, _, pattern, z in cover:
		cost += pattern[0] * np.log2(len(sspace[0])) # Encoding starting locations for each occurence
		cost += pattern[2]*(8+np.log2(len(sspace[0]))) # Encoding reference "shape"
		for start in pattern[1]:
			for i in range(pattern[2]):
				x_diffs.append(z[start+i] - z[start+i-1])
				y_diffs.append(sspace[scale_index][z[start+i]] - sspace[scale_index][z[start+i-1]])

	#cost += huffmanEncodedLength(x_diffs)
	#cost += huffmanEncodedLength(discretize_equal_width(y_diffs))

	#print x_diffs, y_diffs

	return cost

def compute_cover(sspace, scale_patterns):

	mask = np.ones(len(sspace[0]))
	approx = sspace[0].copy()
	patterns_cost = 0

	best_mdl = huffmanEncodedLength(discretize_equal_width(approx, min=np.min(sspace[0]), max=np.max(sspace[0])))
	logging.info('Input Data MDL Score: %d'%best_mdl)
	cover = []

	for scale, (patterns, z) in enumerate(scale_patterns):

		logging.info('Processing scale %d'%(len(scale_patterns)-1-scale))

		best_scale_pattern_score = best_mdl
		best_scale_pattern = None
		best_scale_pattern_index = None

		for i, p in enumerate(patterns):

			temp_mask = mask.copy()
			temp_approx = apply_pattern_model(approx.copy(), temp_mask, p, sspace[len(scale_patterns)-1-scale], z)

			model_cost = huffmanEncodedLength(discretize_equal_width(temp_approx[temp_mask==1], min=np.min(sspace[0]), max=np.max(sspace[0])))
			model_cost_patterns = cover_cost(cover+[(len(scale_patterns)-1-scale, i, p, z)], sspace)

			residual = sspace[0][temp_mask==0] - temp_approx[temp_mask==0]
			residual_cost = huffmanEncodedLength(discretize_equal_width(residual, min=np.min(sspace[0]), max=np.max(sspace[0])))

			#mdl_score = model_cost + model_cost_patterns + patterns_cost + residual_cost
			mdl_score = model_cost + model_cost_patterns + residual_cost

			if mdl_score < best_scale_pattern_score:
				best_scale_pattern_score = mdl_score
				best_scale_pattern = p
				best_scale_pattern_index = i
			print ('Scale', len(scale_patterns)-1-scale, 'Pattern', i, 'MDL Score', mdl_score, 'Model Cost', model_cost+model_cost_patterns+patterns_cost, 'Residual Cost', residual_cost)

		if best_scale_pattern:
			cover.append((len(scale_patterns)-1-scale, best_scale_pattern_index, best_scale_pattern, z))
			#patterns_cost += best_scale_pattern[0]*(best_scale_pattern[2]+1)*(32+np.log2(len(sspace[0])))
			apply_pattern_model(approx, mask, best_scale_pattern, sspace[len(scale_patterns)-1-scale], z)
			print ('[ Pattern applied:', best_scale_pattern_index, 'MDL Score:', best_scale_pattern_score, ']')
			best_mdl = best_scale_pattern_score

	f = open('out.txt', 'w')
	for scale, pattern_index, _, _ in cover:
		f.write('%d %d\n'%(scale, pattern_index))
	f.close()


def computeFrequencies(t):
	''' Compute the frequencies of the symbols in t as a list of tuples
		in the form (symbol, frequency). '''

	frequencies = defaultdict(int)
	for x in t:
		frequencies[x] += 1
	return frequencies


def computeEntropy(t):
	'''Computes the entropy of the given discrete sequence'''

	frequencies = computeFrequencies(t)
	entropy = 0.0

	for f in frequencies.itervalues():
		p = float(f) / len(t)
		entropy += p * log2(1 / p)

	return entropy


def computeHuffmanTree(frequencies):
	''' Computes the Huffman tree for the specified symbol frequencies.
		frequencies is a list of tuple in the form (symbol, frequency). '''

	tree = [HuffmanTreeNode(t[1], t[0], None, None) for t in frequencies.iteritems()]
	heapq.heapify(tree)
	while len(tree) > 1:
		left = heapq.heappop(tree)
		right = heapq.heappop(tree)
		parent = HuffmanTreeNode(left.frequency + right.frequency, None, left, right)
		heapq.heappush(tree, parent)
	return tree[0]  # the root


def computeHuffmanCodeTable(huffmanTree, codeTable, level=0):
	''' Computes the length of the code for each symbol and returns them
		as a dictionary symbol -> codeLength. '''

	if huffmanTree.symbol is not None:
		codeTable[huffmanTree.symbol] = level
	else:
		computeHuffmanCodeTable(huffmanTree.left, codeTable, level + 1)
		computeHuffmanCodeTable(huffmanTree.right, codeTable, level + 1)



def main(DataFrames,parameters):

	logging.basicConfig(level=logging.DEBUG)

	global params
	params = {
	'alphabet_size':int(parameters['alphabet_size']),
	'scale_quantization':str(parameters['scale_quantization']),
	'min_support':int(parameters['min_support']),
	'subsampling_factor':int(parameters['subsampling_factor']),
	'pattern_set_size': int(parameters['pattern_set_size']),
	}

	# Data Loading and z-Normalization
	logging.info('Loading input data.')
	data = np.squeeze(DataFrames.as_matrix())
	data = data[::params['subsampling_factor']]
	data_mean = data.mean()
	data_std = data.std()
	data = (data - data_mean) / data_std

	# Scale Space Quantization
	logging.info('Quantizing scale-space with %s method.' % params['scale_quantization'])

	if params['scale_quantization'] == 'sqrt':
		max_scale = int(np.log2(len(data)**2 / 8))
		scales = [np.sqrt(2 ** i) for i in range(0, max_scale-1)]
	else:
		max_scale = int(np.log2(len(data) / 6.0))
		scales = [2 ** i for i in range(0, max_scale)]

	sspace = None
	if os.path.exists('sspace.pkl'):
		print ('Loading scale space from file.')
		f = open('sspace.pkl','rb')
		sspace = cPickle.load(f)
		f.close()
	else:
		sspace = build_scale_space(data, scales)
		f = open('sspace.pkl','wb')
		cPickle.dump(sspace,f,-1)
		f.close()

	# Candidate Events Generation
	scale_patterns = []
	if os.path.exists('scale_patterns.pkl'):
		print ('Loading scale patterns from file.')
		f = open('scale_patterns.pkl','rb')
		scale_patterns = cPickle.load(f)
		f.close()
	else:
		for s, scale in enumerate(sspace[:]):

			logging.info('Candidate generation on scale %d.'%s)

			patterns, z = candidate_search(scale, data, params['alphabet_size'], 2, 10, params['min_support'], counter=s)

			scale_patterns.append((patterns[:params['pattern_set_size']], z))

			logging.info('Plotting patterns on scale %d.'%s)
			for c, pattern in enumerate(patterns[:params['pattern_set_size']]):
				print ('Plotting', pattern[0], 'patterns.', pattern[4].mean())
				#fig = plot_pattern(data, scale, z, pattern)
				#fig.savefig('scale-%d-pattern-%d.pdf' % (s, c))

		f = open('scale_patterns.pkl','wb')
		cPickle.dump(scale_patterns,f,-1)
		f.close()

	# Compute Cover
	scale_patterns.reverse()
	print ("scale patterns:")
	print (scale_patterns)
	compute_cover(sspace, scale_patterns)
