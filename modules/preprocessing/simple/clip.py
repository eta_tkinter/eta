import pandas as pd

class Module():
	def __init__(self):
		self.moduleName = 'clip'

	def getParameters(self):
		from collections import OrderedDict # for keeping options in order
		settings = OrderedDict([
			('lower', 'float'),
			('upper', 'float'),
		])
		return settings

	def run(self,data,parameters):
		lowerValue = float(parameters['lower'])
		upperValue = float(parameters['upper'])

		newData = data.clip(lowerValue, upperValue)

		returned = {
			'newDataFrame': 'No',
			'data': newData,
			'name_addition': self.moduleName
		}

		return returned
