from __future__ import division # bugfix

import numpy as np
import pandas as pd

from collections import OrderedDict # for keeping options in order

class Module():
    def __init__(self):
        self.moduleName = 'resampler'

    def getParameters(self):
        settings = OrderedDict([
            ('timestamp_field', 'columns'),
            ('resample_on', ['AS','A','Q','MS','W','D','H','min','S','ms','us','N']),
            ('time_per_sample','integer'),
            ('resample_aggregation', ['sum','mean','std','sem','max','min','median','first','last','ohlc'])
            # ('resample_aggregation', ['anomalies','average','cumsum','mean','median','std','sum','var','max','min'])
        ])
        return settings

    def getDefaults(self):
        defaults = {
            'timestamp_field': None,
            'time_per_sample': 1,
            'resample_on': 'S',
            'resample_aggregation': 'mean',
        }
        return defaults

    def run(self,data,parameters):
        timestamp_field = parameters['timestamp_field']
        time_per_sample = parameters['time_per_sample']
        resample_on = parameters['resample_on']
        resample_aggregation = parameters['resample_aggregation']
        resample_field = str(time_per_sample) + resample_on

        # Copy the data to not change it
        data.set_index(pd.DatetimeIndex(data[timestamp_field]),inplace=True)

        resampledData = data.resample(resample_field).apply(resample_aggregation)
        resampledData[timestamp_field] = resampledData.index.values
        resampledData = resampledData.reset_index(drop=True)

        returned = {
            'newDataFrame': True, # diffent data sizes
            'data': resampledData,
            'name_addition': timestamp_field + resample_field
        }

        return returned
        # RETURN DATA, MESSAGE , IF WHOLE DATASET NEEDS TO BE REPLACED

# Alias    Description
# B    business day frequency
# C    custom business day frequency (experimental)
# D    calendar day frequency
# W    weekly frequency
# M    month end frequency
# BM    business month end frequency
# CBM    custom business month end frequency
# MS    month start frequency
# BMS    business month start frequency
# CBMS    custom business month start frequency
# Q    quarter end frequency
# BQ    business quarter endfrequency
# QS    quarter start frequency
# BQS    business quarter start frequency
# A    year end frequency
# BA    business year end frequency
# AS    year start frequency
# BAS    business year start frequency
# BH    business hour frequency
# H    hourly frequency
# T, min    minutely frequency
# S    secondly frequency
# L, ms    milliseconds
# U, us    microseconds
# N    nanoseconds
