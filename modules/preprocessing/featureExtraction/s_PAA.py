import numpy as np
import pandas as pd
import math
import itertools

class Module():
	def __init__(self):
		self.moduleName = 'PAA'

	def getDefaults(self):
		settings = {
			'frame_size' : '20',
			'same_length_as_source_df':'False'
		}
		return settings

	def getParameters(self):
		settings = {
			'frame_size' : 'integer',
			'same_length_as_source_df':['True','False']
		}
		return settings

	def run(self,data,parameters=None):
		newDataFrame = pd.DataFrame()
		frameSize = parameters['frame_size']
		newData = {}
		lastAvg = {}
		for column in data:
			newData[column] = []
			lastAvg[column] = []
			l = []
			for index, value in data[column].iteritems():
				l.append(value)
				mod = index % frameSize
				if mod == 0 and index > 0:
					avg = [np.mean(l)]
					del lastAvg[column][:]
					#print avg
					if parameters['same_length_as_source_df'] == 'True':
						avgList = [avg] * frameSize
						newData[column].append(avgList)
					else:
						newData[column].append(avg)
					del l[:]
				if parameters['same_length_as_source_df'] == 'True':
					lastAvg[column].append(value)
			listoflists = [item for sublist in newData[column] for item in sublist]
			if parameters['same_length_as_source_df'] == 'True':
				newData[column] = list(itertools.chain(*listoflists))
			else:
				newData[column] = listoflists

			newDataFrame[column] = newData[column]
		if parameters['same_length_as_source_df'] == 'True':
			fillerDf = pd.DataFrame()
			for column in lastAvg:
				amount = len(data[column]) - len(newData[column])
				avg = np.mean(lastAvg[column])
				l =  amount * [np.mean(lastAvg[column])]
				fillerDf[column] = l
			newDataFrame = newDataFrame.append(fillerDf,ignore_index=True)

		return newDataFrame
