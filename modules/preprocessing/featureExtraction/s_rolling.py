import numpy as np
import pandas as pd
from sklearn.preprocessing import Imputer

class Module():
	def __init__(self):
		self.moduleName = "Rolling"

	def getDefaults(self):
		return {
			'feature':'mean',
			'window_size':'25'
		}
	def getParameters(self):
		#key => value
		#value is list => dropdown
		#value is str(float) => empty field
		settings = {
			'feature':['mean','sum','median','min','max','std'],
			#'amount':'float',
			'window_size': 'integer',
		}
		return settings

	def run(self,data,parameters=None):
		window_size = parameters['window_size']
		if parameters['feature'] == 'mean':
			return data.rolling(window=window_size).mean()
		elif parameters['feature'] == 'sum':
			return data.rolling(window=window_size).sum()
		elif parameters['feature'] == 'median':
			return data.rolling(window=window_size).median()
		elif parameters['feature'] == 'min':
			return data.rolling(window=window_size).min()
		elif parameters['feature'] == 'max':
			return data.rolling(window=window_size).max()
		elif parameters['feature'] == 'std':
			return data.rolling(window=window_size).std()
