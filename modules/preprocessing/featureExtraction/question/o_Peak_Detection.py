import numpy as np
import pandas as pd
import math
from scipy import signal
import peakutils

class Module():
	'''Based on http://pythonhosted.org/PeakUtils/'''
	def __init__(self):
		self.moduleName = 'peak_detection'

	def getParameters(self):
		from collections import OrderedDict # for keeping options in order
		settings = OrderedDict([
			('column', 'columns'),
			('peak_detection_algorithm', ['SciPy','Based on Matplotlib']),
			('peakIndex_as_dataFrameIndex', ['True','False']),
			('min_distance(for MPL)', 'integer'),
		])
		return settings

	def getDefaults(self):
		return {
			'peakIndex_as_dataFrameIndex': 'True',
			'min_distance(for MPL)':'50',
			'peak_detection_algorithm':'Based on Matplotlib'
		}

	def run(self,data,parameters=None):
		self.data = data
		self.column = str(parameters['column'])

		newData = pd.DataFrame()
		if parameters['peak_detection_algorithm'] == 'SciPy':
			peakIndices = signal.find_peaks_cwt(self.data[self.column].as_matrix(), np.arange(1,10))
		else:
			peakIndices = peakutils.indexes(self.data[self.column].as_matrix(), thres=0.02/max(self.data[self.column].as_matrix()),min_dist=parameters['min_distance(for MPL)'])
			print (peakIndices)
		baseline = self.generateMean()
		print ("Amount of peaks")
		print ("--------------")
		print (len(peakIndices))
		peakIndexLeft = []
		peakIndexRight = []
		areaUnderCurve = []
		for peakIndex in peakIndices:
			print ("peakIndex")
			print (peakIndex)
			print ("leftIndex")
			leftIndex = self.findLeftIndex(baseline,peakIndex)
			peakIndexLeft.append(leftIndex)
			print ("rightIndex")
			rightIndex = self.findRightIndex(baseline,peakIndex)
			peakIndexRight.append(rightIndex)

			areaUnderCurve.append(self.generateAreaUnderCurve(leftIndex,rightIndex,baseline))
			print ("===============")
			pass
		#find left crossing with
		print
		#find right crossing with baseline

		newData[self.column+'_height'] = self.data[self.column][peakIndices]
		newData[self.column+'_width'] = np.subtract(peakIndexRight,peakIndexLeft)
		newData[self.column+'_area'] = areaUnderCurve
		if parameters['peakIndex_as_dataFrameIndex'] == 'False':
			newData.reset_index(inplace=True)
		#print (newData)
		print (newData)
		return newData

	def findLeftIndex(self,baseline,peakIndex):
		found = False

		currentIndex = peakIndex
		while not found:
			currentIndex = currentIndex-1
			if self.data[self.column][currentIndex] < 1.01*baseline:
				found = True
				return currentIndex
			pass

	def findRightIndex(self,baseline,peakIndex):
		found = False

		currentIndex = peakIndex
		while not found:
			currentIndex = currentIndex+1
			if self.data[self.column][currentIndex] < 1.01*baseline:
				found = True
				return currentIndex
			pass

	def generateMean(self):
		'''Generate mean within 2* std'''
		std = self.data[self.column].std()
		tmpMean = self.data[self.column].mean()
		#inside 2x std
		lowerBound = tmpMean -2*std
		upperBound = tmpMean +2*std
		meanData = self.data[self.column][(self.data[self.column] > lowerBound) & (self.data[self.column] < upperBound)]

		return  meanData.mean()

	def generateAreaUnderCurve(self,leftBound,rightBound,baseline):
		valuesUnderCurve = self.data[self.column][leftBound:rightBound+1]
		print (valuesUnderCurve)
		areaUnderCurveIncludingBaseline = np.trapz(valuesUnderCurve)
		print (areaUnderCurveIncludingBaseline)
		return np.abs(areaUnderCurveIncludingBaseline)
		#np.trapz([1,2,3])
