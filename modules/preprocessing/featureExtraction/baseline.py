import numpy as np
import pandas as pd
from sklearn.preprocessing import Imputer

class Module():
    def __init__(self):
        self.moduleName = 'baseline'

    def run(self,data,parameters):
        newData = pd.DataFrame()

        for column in data:
            std = data[column].std()
            tmpMean = data[column].mean()
            #inside 2x std
            lowerBound = tmpMean -2*std
            upperBound = tmpMean +2*std
            meanData = data[column][(data[column] > lowerBound) & (data[column] < upperBound)]
            mean = meanData.mean()
            tmpList = [mean for i in range(len(data))]

            newData[column] = tmpList

        return newData
