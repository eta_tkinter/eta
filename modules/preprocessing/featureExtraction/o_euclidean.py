import numpy as np
import pandas as pd
import math

class Module():
	def __init__(self):
		self.moduleName = 'euclidean'

	def getParameters(self):
		from collections import OrderedDict # for keeping options in order
		settings = OrderedDict([
			('column_1', 'columns'),
			('column_2', 'columns')
		])
		return settings

	def run(self,data,parameters):
		c1 = parameters['column_1']
		c2 = parameters['column_2']
		name = '{}-{}'.format(c1, c2)
		newData = pd.DataFrame()
		newData[name] = data[c2] - data[c1]
		newData = newData.abs()

		return newData
