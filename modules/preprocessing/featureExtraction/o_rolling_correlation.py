import numpy as np
import pandas as pd

class Module():
	def __init__(self):
		self.moduleName = 'rolling_correlation'

	def getParameters(self):
		settings = {
			'column1':'columns',
			'column2':'columns',
			'window_size': 'integer',
			# 'method': ['pearson','kendall','spearman']
		}
		return settings

	def run(self,data,parameters):
		c1 = parameters['column1']
		c2 = parameters['column2']
		window_size = parameters['window_size']
		# method = parameters['method']

		newData = pd.DataFrame()
		colName = c1 + '_' + c2
		newData[colName] = data[c1].rolling(window=window_size).corr(other=data[c2])
		return newData
