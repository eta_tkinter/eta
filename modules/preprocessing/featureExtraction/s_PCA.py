import numpy as np
import pandas as pd
import math
from sklearn.decomposition import PCA

class Module():
	def __init__(self):
		self.moduleName = 'PCA'

	def getParameters(self):
		settings = {
			'n_components' : 'integer',
		}
		return settings

	def run(self,data,parameters=None):
		components = parameters['n_components']
		pca = PCA(n_components=components)

		formattedData = [ [] for x in range(0,components) ]
		newData = pca.fit_transform(data)
		for value in newData:
			for columnCount in range(0,components):
				formattedData[columnCount].append(value[columnCount])
				pass
			pass
		#print formattedData

		newData = pd.DataFrame()
		for column in range(0,components):
			newData[column] = formattedData[column]
		return newData
