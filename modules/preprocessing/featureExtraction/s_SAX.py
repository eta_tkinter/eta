import numpy as np
import pandas as pd
import math

class Module():
	def __init__(self):
		self.moduleName = "SAX"

	def getDefaults(self):
		settings = {
		'frame_size' : '20',
		'alphabet_size':'5',
		'same_length_as_source_df':'False'
		}
		return settings

	def getParameters(self):
		settings = {
			'frame_size' : 'integer',
			'alphabet_size':['3','4','5','6','7','8','9'],
			'same_length_as_source_df':['True','False']
		}
		return settings

	def run(self,data,parameters=None):

		lookupBreakpoints = {
			3: [-0.43,0.43],
			4: [-0.67,0,0.67],
			5: [-0.84,-0.25,0.25,0.84],
			6: [-0.97,-0.43,0,0.43,0.97],
			7: [-1.07,-0.57,-0.18,0.18,0.57,1.07],
			8: [-1.15,-0.67,-0.32,0,0.32,0.67,1.15],
			9: [-1.22,-0.76,-0.43,-0.14,0.14,0.43,0.76,1.22],
			10: [-1.28, -0.84, -0.52, -0.25, 0 , 0.25 ,0.52, 0.84, 1.28],
		}

		lookupAlphabet = {
			3: ['a','b','c'],
			4: ['a','b','c','d'],
			5: ['a','b','c','d','e'],
			6: ['a','b','c','d','e','f'],
			7: ['a','b','c','d','e','f','g'],
			8: ['a','b','c','d','e','f','g','h'],
			9: ['a','b','c','d','e','f','g','h','i'],
			10: ['a','b','c','d','e','f','g','h','i','j'],
		}

		frameSize = parameters['frame_size']
		newData = {}

		lastLetter = {}
		for column in data:
			newData[column] = []
			l = []
			for index, value in data[column].iteritems():
				l.append(value)
				mod = index % frameSize
				#print mod
				if mod == 0 and index != 0:
					avg = np.mean(l)

					## SEARCH CORRESPONDING LETTER #
					if avg < lookupBreakpoints[int(parameters['alphabet_size'])][0]: #smallest element -> first letter
						letter = lookupAlphabet[int(parameters['alphabet_size'])][0]
					else:
						letterFound = False
						posLetter = 1
						while letterFound is False:
							#print "posLetter"
							#print posLetter
							#print '---------'
							if (int(parameters['alphabet_size'])-1) != (posLetter): #zolang niet op het einde
								if avg >= lookupBreakpoints[int(parameters['alphabet_size'])][posLetter-1] and avg < lookupBreakpoints[int(parameters['alphabet_size'])][posLetter]:
									#print "> vorige, kleiner dan huidige"
									letterFound = True
									letter = lookupAlphabet[int(parameters['alphabet_size'])][posLetter]
								else:
									#print "7"
									posLetter += 1
									pass
							else:
								#print "4"
								letterFound = True
								letter = lookupAlphabet[int(parameters['alphabet_size'])][posLetter]
							#print '////////////////////////////////////////'

					#print saxList
					if parameters['same_length_as_source_df'] == 'True':
						saxList = [letter] * frameSize
						newData[column].append(saxList)
					else:
						newData[column].append(letter)

					del l[:]
					#print newData[column]
					newData[column] = [item for sublist in newData[column] for item in sublist]
					lastLetter[column] = letter

		newData = pd.DataFrame(newData)
		if parameters['same_length_as_source_df'] == 'True':
			#pri
			fillerDf = pd.DataFrame()
			for column in lastLetter:
				l = ((len(data)- len(newData))) * [lastLetter[column]]
				fillerDf[column] = l
			newData = newData.append(fillerDf,ignore_index=True)
		return newData
		#print newData
		#print type(frameSize)
		#print type(frameStep)
		#print len(data.index)
