import numpy as np
import pandas as pd

from collections import OrderedDict # for keeping options in order

from sklearn.preprocessing import MinMaxScaler

class Module():
	def __init__(self):
		self.moduleName = 'normalize'

	def run(self,data,parameters):
		scaler = MinMaxScaler(feature_range=[0,1],copy=True)
		X = scaler.fit_transform(data)
		newData = pd.DataFrame(data=X,columns=data.columns.values)

		returned = {
			'newDataFrame': 'No',
			'data': newData,
			'name_addition': 'norm'
		}

		return returned
