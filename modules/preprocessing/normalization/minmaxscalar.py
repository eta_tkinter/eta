import numpy as np
import pandas as pd

from collections import OrderedDict # for keeping options in order
import pickle # for exportin and importing parameters
import os.path # for checking if a file exist

from sklearn.preprocessing import MinMaxScaler

class Module():
	def __init__(self):
		self.moduleName = 'minmaxscalar'

	def getParameters(self):
		settings = OrderedDict([
			('min', 'integer'),
			('max', 'integer'),
		])
		return settings

	def getDefaults(self):
		defaults = {
			'min': '0',
			'max': '1',
		}
		return defaults

	def run(self,data,parameters):
		minValue = parameters['min']
		maxValue = parameters['max']

		maskedData = data
		scaler = MinMaxScaler(feature_range=[minValue,maxValue],copy=True)
		X = scaler.fit_transform(maskedData)
		newData = pd.DataFrame(data=X,columns=maskedData.columns.values)

		returned = {
			'newDataFrame': 'No',
			'data': newData,
			'name_addition': self.moduleName
		}

		return returned
