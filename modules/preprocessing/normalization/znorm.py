from __future__ import division # bugfix

import numpy as np
import pandas as pd

from collections import OrderedDict # for keeping options in order

from scipy import stats

# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.zscore.html

class Module():
	def __init__(self):
		self.moduleName = 'znorm'

	def run(self,data,parameters):
		newData = pd.DataFrame()

		for column in data:
			newData[column] = stats.zscore(data[column], axis=0, ddof=0)

		returned = {
			'newDataFrame': False,
			'data': newData,
			'name_addition': 'znorm'
		}

		return returned
		# RETURN DATA, MESSAGE , IF WHOLE DATASET NEEDS TO BE REPLACED
