import numpy as np
import pandas as pd
import math
from sklearn import tree
from sklearn import metrics

from collections import OrderedDict

class Module():
    def __init__(self):
        self.moduleName = "CART"
        self.machineLearningType = 'classification'

    def getParameters(self, case = None):
        if case == 'fit':
            return self.getFitParameters()
        elif case == 'predict':
            return None
        elif case == 'score':
            return self.getScoreParameters()
        else:
            return False

    def getFitParameters(self):
        settings = {
            'target' : 'columns',
        }
        return settings

    def getScoreParameters(self):
        scoreParameters = OrderedDict([
            ('control', 'columns'),
            ('predicted', 'columns'),
        ])
        return scoreParameters

    def fit(self, data, parameters=None):
        target = parameters['target']
        y = data[target]
        del data[target]
        X = data

        clf = tree.DecisionTreeClassifier()
        clf = clf.fit(X, y)
        return clf

    def predict(self, data, parameters, model):
        newData = pd.DataFrame()
        newData =  model.predict(data)
        return newData

    def score(self, data, parameters, model):
        control = parameters['control']
        predicted = parameters['predicted']
        yTrue = data[control]
        yPredicted = data[predicted]

        score = {
            'accuracy:': metrics.accuracy_score(yTrue, yPredicted),
            'confusion_matrix': metrics.confusion_matrix(yTrue, yPredicted),
            # 'score': model.score(yTrue, yPredicted)
        }

        return score
