import pandas as pd
import numpy as np

from sklearn.linear_model import LinearRegression
from collections import OrderedDict

class Module():
    def __init__(self):
        self.moduleName = 'LinearRegression'
        self.machineLearningType = 'regression'

    def getParameters(self, case = None):
        if case == 'fit':
            return self.getFitParameters()
        elif case == 'predict':
            return None
        elif case == 'score':
            return self.getScoreParameters()
        else:
            return False

    def getFitParameters(self):
        scoreParameters = OrderedDict([
            ('target', 'columns'),
        ])
        return scoreParameters

    def getScoreParameters(self):
        scoreParameters = OrderedDict([
            ('control', 'columns'),
            ('predicted', 'columns'),
        ])
        return scoreParameters

    def fit(self, data, parameters):
        target = parameters['target']
        y = data[target]
        del data[target]
        X = data

        model = LinearRegression()
        model = model.fit(X, y)
        return model

    def predict(self, data, parameters, model):
        newData = pd.DataFrame()
        newData = model.predict(data)
        return newData

    def score(self, data, parameters, model):
        control = parameters['control']
        predicted = parameters['predicted']
        yTrue = data[control]
        yPredicted = data[predicted]
        score = model.score(yTrue, yPredicted)
        return score
