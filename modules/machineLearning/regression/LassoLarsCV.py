import numpy as np
import pandas as pd
import math
from sklearn.linear_model import LassoLarsCV
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from collections import OrderedDict

class Module():
    def __init__(self):
        self.moduleName = 'LassoLarsCV'
        self.machineLearningType = 'regression'

    def getParameters(self, case = None):
        if case == 'fit':
            return self.getFitParameters()
        elif case == 'predict':
            return None
        elif case == 'score':
            return self.getScoreParameters()
        else:
            return False

    def getFitParameters(self):
        settings = OrderedDict([
            ('target', 'columns'),
            ('normalize', 'boolean')
        ])
        return settings

    def getScoreParameters(self):
        scoreParameters = OrderedDict([
            ('control', 'columns'),
            ('predicted', 'columns'),
        ])
        return scoreParameters

    def fit(self,data,parameters):
        target = parameters['target']
        y = data[target]
        del data[target]
        X = data

        clf = LassoLarsCV(normalize=bool(parameters['normalize']))
        MSE = clf.fit(X, y)
        return clf

    def predict(self, data, parameters, model):
        newData = pd.DataFrame()
        newData = model.predict(data)
        return newData

    def score(self, data, parameters, model):
        control = parameters['control']
        predicted = parameters['predicted']
        yTrue = data[control]
        yPredicted = data[predicted]
        score = {
            'r2_score:': r2_score(yTrue, yPredicted),
            'mean_squared_error:': mean_squared_error(yTrue, yPredicted),
        }
        return score
