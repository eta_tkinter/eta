import numpy as np
import pandas as pd
from sklearn.cluster import SpectralClustering

class machineLearning_Module():
	def __init__(self):
		pass
	def getName(self):
		return "SpectralClustering"
	def getOptionDefaults(self):
		settings = {
		'affinity':'nearest_neighbors',
		}
		return settings
	def getOptions(self):
		#key => value
		#value is list => dropdown
		#value is str(float) => empty field
		settings = {
		'affinity':['nearest_neighbors', 'precomputed', 'rbf'],
		}
		return settings
	def run(self,DataFrames,parameters=None):
		maskedData = DataFrames['maskedData']
		spectral = SpectralClustering().fit(maskedData)

		newData = pd.DataFrame()
		newData['label'] = spectral.labels_[:]
		return newData
