import numpy as np
import pandas as pd
from sklearn.cluster import KMeans

class Module():
	def __init__(self):
		self.moduleName = 'KMeans'
		self.machineLearningType = 'classification'

	def getParameters(self):
		settings = {
			'n_clusters' : 'integer',
		}
		return settings

	def run(self, data, parameters=None):
		k_means = KMeans(n_clusters=parameters['n_clusters']).fit(data)

		newData = pd.DataFrame()
		newData['label'] = k_means.labels_[:]
		return newData
