from sklearn.datasets import load_iris

from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from numpy.random import RandomState

import numpy as np
import pylab as pl
from itertools import cycle

class Module():
    def __init__(self):
        self.moduleName = 'scikit'

    def fit(self, data, parameters):
        # http://www.astroml.org/sklearn_tutorial/general_concepts.html

        # print("Supervised:")
        iris = load_iris()
        n_samples, n_features = iris.data.shape
        # print(iris)
        X, y = iris.data, iris.target # X is data, y is labels
        clf = LinearSVC()
        # print(clf)
        clf = clf.fit(X, y)
        # print(clf.coef_)
        # print(clf.intercept_)
        X_new = [[ 5.0,  3.6,  1.3,  0.25]]
        y_new = clf.predict(X_new)
        print(y_new)

        clf2 = LogisticRegression().fit(X, y)
        # print(clf2)
        y2_new_proba = clf2.predict_proba(X_new)
        # print(y2_new_proba) # Chances off class
        y2_new = clf2.predict(X_new)
        # print(y2_new)

        # Notable implementations of classifiers:
        # http://www.astroml.org/sklearn_tutorial/general_concepts.html#notable-implementations-of-classifiers

        # Notable implementations of regression models:
        # http://www.astroml.org/sklearn_tutorial/general_concepts.html#regression

        # print("Unsupervised:")
        pca = PCA(n_components=2, whiten=True).fit(X)
        # print(pca)
        # print(pca.components_) # Singular vectors
        # print(pca.explained_variance_ratio_)
        # print(pca.explained_variance_ratio_.sum())
        X_pca = pca.transform(X)

        np.round(X_pca.mean(axis=0), decimals=5)
        np.round(X_pca.std(axis=0), decimals=5)
        np.round(np.corrcoef(X_pca.T), decimals=5)

        def plot_2D(data, target, target_names):
            colors = cycle('rgbcmykw')
            target_ids = range(len(target_names))
            pl.figure()
            for i, c, label in zip(target_ids, colors, target_names):
                pl.scatter(data[target == i, 0], data[target == i, 1], c=c, label=label)
            pl.legend()
            pl.show()

        # plot_2D(X_pca, iris.target, iris.target_names)

        # print("Clustering:")
        rng = RandomState(42)
        kmeans = KMeans(n_clusters=3, random_state=rng).fit(X_pca)
        np.round(kmeans.cluster_centers_, decimals=2)
        kmeans.labels_[:10]
        kmeans.labels_[-10:]

        # plot_2D(X_pca, kmeans.labels_, ["c0", "c1", "c2"])

        # Notable implementations of clustering models
        # http://www.astroml.org/sklearn_tutorial/general_concepts.html#notable-implementations-of-clustering-models

        print("Measuring:")
        indices = np.arange(n_samples)
        RandomState(42).shuffle(indices)
        X = iris.data[indices]
        y = iris.target[indices]
        split = (n_samples * 2) / 3
        X_train, X_test = X[:split], X[split:]
        y_train, y_test = y[:split], y[split:]
        print(X_train.shape)
        print(X_test.shape)
        print(y_train.shape)
        print(y_test.shape)
        clf = LinearSVC().fit(X_train, y_train)
        print(np.mean(clf.predict(X_test) == y_test))
