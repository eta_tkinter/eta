import numpy as np
import pandas as pd
import math
from sklearn.linear_model import RidgeCV
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
class machineLearning_Module():
	def __init__(self):
		pass
	def getName(self):
		return "RidgeCV"
	def getOptions(self):
		#key => value
		#value is list => dropdown
		#value is str(float) => empty field
		settings = {
		'column_to_fit' : 'columns',
		'normalize':['True','False']
		}
		return settings
	def fit(self,dataFrames,parameters=None):
		maskedData = dataFrames['maskedData']
		data = dataFrames['data']

		clf = RidgeCV(store_cv_values=True, normalize=bool(parameters['normalize']))
		MSE = clf.fit(maskedData, data[parameters['column_to_fit']])

		return clf
	def predict(self,dataFrames,model):
		maskedData = dataFrames['maskedData']
		#cv = pd.DataFrame(model.cv_values_)

		newData = pd.DataFrame()
		newData['fitted'] =  model.predict(maskedData)

		return newData
	def metrics(self,yPredicted,yTest):
		metrics = {
		'r2_score:':r2_score(yTest, yPredicted),
		'mean_squared_error:':mean_squared_error(yTest, yPredicted),
		}

		return metrics
