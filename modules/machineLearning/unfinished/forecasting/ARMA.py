from __future__ import print_function

import numpy as np
import pandas as pd
import math

from scipy import stats
import matplotlib.pyplot as plt
import statsmodels.api as sm

from statsmodels.graphics.api import qqplot

class Module():
    def __init__(self):
        self.moduleName = 'ARMA'
        self.machineLearningType = 'forecasting'

    # def getParameters(self):
    #     from collections import OrderedDict
    #     settings = OrderedDict([
    #         ('target', 'columns'),
    #     ])
    #     return settings

    def fit(self, data, parameters):
        # dta = sm.datasets.sunspots.load_pandas().data
        # dta.index = pd.Index(sm.tsa.datetools.dates_from_range('1700', '2008'))
        # del dta["YEAR"]
        # print(data)

        data.index = pd.Index(sm.tsa.datetools.dates_from_range('1800', '1982'))
        model = sm.tsa.ARMA(data, (3,0)).fit()
        # predict_sunspots = arma_mod30.predict('1990', '2012', dynamic=True)

        # data.plot(figsize=(12,8));

        print(model)
        return model

    def predict(self, data, parameters, model):
        # dta = sm.datasets.sunspots.load_pandas().data
        # dta.index = pd.Index(sm.tsa.datetools.dates_from_range('1700', '2008'))
        # del dta["YEAR"]

        data.index = pd.Index(sm.tsa.datetools.dates_from_range('1800', '1982'))
        newData = pd.DataFrame()
        # newData = model.predict('1990', '2012', dynamic=True)

        fig, ax = plt.subplots(figsize=(12, 8))
        ax = data.ix['1900':].plot(ax=ax)
        fig = model.plot_predict('1960', '2016', dynamic=True, ax=ax, plot_insample=False)
        plt.show()
        return newData
