from sklearn.linear_model import LinearRegression, Lasso, LassoLarsCV
from sklearn.model_selection import cross_val_score
from sklearn.metrics import r2_score, make_scorer

class Module():
    def __init__(self):
        self.moduleName = 'kevin'
        self.machineLearningType = 'cross_validation'

    def getParameters(self):
        from collections import OrderedDict # for keeping options in order
        settings = OrderedDict([
            ('target','columns')
        ])
        # print(settings)
        return settings

    def fit(self, data, parameters):
        print("Fitting")
        target = parameters['target']
        data = data[data[target] >= 0]
        print(data)
        print(data[data[target] < 0])
        X = data.ix[:,data.columns != target]
        y = data[target]
        clf = LinearRegression()
        # clf = LassoLarsCV()
        clf.fit(X, y)
        print(clf)
        print(clf.coef_)
        r2_scorer = make_scorer(r2_score)
        score = cross_val_score(clf, X, y, scoring=r2_scorer).mean()
        print(score)
        return clf

    def predict(self, data, parameters, model):
        print("Predicting")
        print(model)
        # target = parameters['target']
        target = 'Berth occupation in hours'
        X = data.ix[:,data.columns != target]
        y = data[target]
        X_new = 0
        newData = model.predict(X_new)
