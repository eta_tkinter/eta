import matplotlib
matplotlib.use('TkAgg')
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from matplotlib.ticker import FuncFormatter

import pandas as pd

from collections import OrderedDict

import seaborn as sns
plt.style.use('ggplot')

class Module():
	def __init__(self):
		self.moduleName = 'time_multiple' # change this to the module name

	def getParameters(self):
		settings = OrderedDict([
			('index', 'columns'),
			('title', 'string'),
			('xlabel', ['Index','None']),
			('ylabel', ['Column','None']),
			('share-axis', ['Yes','No']),
			('edge', ['Yes','No']),
			('dpi', 'integer'),
			('figsize_height', 'integer'),
			('figsize_width', 'integer'),
		])
		return settings

	def getDefaults(self):
		defaults = {
			'edge': 'Yes',
			'title': None,
			'xlabel': 'Index',
			'ylabel': 'Column',
			'dpi': '80',
			'figsize_height': 4,
			'figsize_width': 10,
			'share-axis': 'No',
			'figsize_width': 'time'
		}
		return defaults

	def run(self, data, parameters):
		# Get the parameters into variables
		edge = parameters['edge']
		title = parameters['title']
		xlabel = parameters['xlabel']
		ylabel = parameters['ylabel']
		dpi = int(parameters['dpi'])
		index = parameters['index']
		share_axis = parameters['share-axis']
		figsize_width = parameters['figsize_width']
		figsize_height = parameters['figsize_height']

		# Use the seaborn style
		self.use_seaborn_style()

		# Set the automatic date locator for xticks
		locator = dates.AutoDateLocator()
		# Set the automatic date formatter for xlabels
		formatter = dates.AutoDateFormatter(locator)
		scaled = {
			365.0  : '%Y',
			30.	: '%b %Y',
			1.0	: '%b %d %Y',
			1./24. : '%H:%M:%S',
			# 1. / (24. * 60.): '%H:%M:%S.%f',
		}
		formatter.scaled[1/(24.*60.)] = FuncFormatter(self.my_format_function) # only show min and sec

		# figure = Figure(figsize=(14, 5), dpi=dpi)
		figure = plt.figure(figsize=(figsize_width, figsize_height), dpi=dpi)

		data[index] = pd.to_datetime(data[index])
		data = data.set_index(data[index])

		subplot = (len(data.columns)-1) * 100 + 10

		axes = []
		i = 0
		for column in data:
			if column == index:
				if xlabel == 'Index':
					xlabel_label = column
			else:
				# Add a subplot, with or without shared axis
				# if i == 1 or share_axis == 'No':
				# 	axes.append(self.figure.add_subplot(subplot+i))
				# else:
				# 	axes.append(self.figure.add_subplot(subplot+i,sharex=self.axes[0], sharey=self.axes[0]))
				axes.append(figure.add_subplot(subplot+(i+1)))

				figure.subplots_adjust(left=0.10, bottom=0.3,right=0.95,top=0.85,hspace=0.7)

				axes[i].plot_date(data[index],data[column],'-')

				# Add a ylabel
				if ylabel == 'Column':
					axes[i].set_ylabel(column)

				if i == 0 and title != None:
					axes[i].set_title(title)

				axes[i].set_xticklabels(data[index],rotation=45)
				axes[i].xaxis.set_major_locator(locator)
				axes[i].xaxis.set_major_formatter(formatter)

				i += 1
			# End for loop

		# Set xlabel after last plot
		if xlabel == 'Index':
			axes[i-1].set_xlabel(xlabel_label)

		# plt.figure(figure)
		# plt.show(figure)
		# plt.draw()
		# figure.show()

		# if option == '1':
		# 	plt.show()
		# 	return False
		# elif option == '2':
		# 	return figure
		# else:
		# 	return False

		return figure


	def use_seaborn_style(self):
		sns.palplot(sns.color_palette("muted"))
		sns.set_style(
			{'axes.axisbelow': True,
			 'axes.edgecolor': '.8',
			 'axes.facecolor': 'white',
			 'axes.grid': True,
			 'axes.labelcolor': '.15',
			 'axes.linewidth': 1.0,
			 'figure.facecolor': 'white',
			 'font.family': [u'sans-serif'],
			 'font.sans-serif': [u'Arial',
			  u'Liberation Sans',
			  u'Bitstream Vera Sans',
			  u'sans-serif'],
			 'grid.color': '.8',
			 'grid.linestyle': u'-',
			 'image.cmap': u'Greys',
			 'legend.frameon': False,
			 'legend.numpoints': 1,
			 'legend.scatterpoints': 1,
			 'lines.solid_capstyle': u'round',
			 'text.color': '.15',
			 'xtick.color': '.15',
			 'xtick.direction': u'out',
			 'xtick.major.size': 0.0,
			 'xtick.minor.size': 0.0,
			 'ytick.color': '.15',
			 'ytick.direction': u'out',
			 'ytick.major.size': 0.0,
			 'ytick.minor.size': 0.0}
		)

	def my_format_function(self, x, pos=None):
		x = dates.num2date(x)
		if pos == 0: # first tick
			# fmt = '%d-%m-%Y %H:%M:%S'
			fmt = '%H:%M:%S'
		else:
			fmt = '%M:%S'
		label = x.strftime(fmt)
		return label
