import matplotlib
matplotlib.use('TkAgg')
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from matplotlib.ticker import FuncFormatter

import pandas as pd

from collections import OrderedDict

import seaborn as sns
plt.style.use('ggplot')

class Module():
	def __init__(self):
		self.moduleName = 'time_single'

	def getParameters(self):
		settings = OrderedDict([
			('index', 'columns'),
			('title', 'string'),
			('xlabel', ['Index','None']),
			('legend', 'boolean'),
			('dpi', 'integer'),
			('figsize_width', 'integer'),
			('figsize_height', 'integer'),
		])
		return settings

	def getDefaults(self):
		defaults = {
			'index': 'time',
			'title': None,
			'xlabel': 'Index',
			'legend': True,
			'dpi': 80,
			'figsize_width': 10,
			'figsize_height': 4,
		}
		return defaults

	def run(self, data, parameters):

		# Get the parameters into variables
		title = parameters['title']
		xlabel = parameters['xlabel']
		legend = parameters['legend']
		dpi = int(parameters['dpi'])
		figsize_height = parameters['figsize_height']
		figsize_width = parameters['figsize_width']
		figsize = (figsize_width, figsize_height)
		index = parameters['index']

		# Use the seaborn style
		self.use_seaborn_style()

		figure = plt.figure(figsize=figsize, dpi=dpi)

		# Set the automatic date locator for xticks
		locator = dates.AutoDateLocator()
		# Set the automatic date formatter for xlabels
		formatter = dates.AutoDateFormatter(locator)
		formatter.scaled[1/(24.*60.)] = '%M:%S' # only show min and sec

		data[index] = pd.to_datetime(data[index])
		data = data.set_index(data[index])

		for column in data:
			if column == index:
				pass
			else:
				ax = figure.add_subplot(111)
				ax.plot_date(data[index],data[column],'-', label=column)

				figure.subplots_adjust(left=0.10, bottom=0.3,right=0.95,top=0.85,hspace=0.3)

				# Add a legend
				if legend == 'Yes':
					 ax.legend(loc="upper right")

				if xlabel != None:
					ax.set_xlabel(index)

				if title != None:
					ax.set_title(title)

				ax.set_xticklabels(data[index],rotation=45)
				ax.xaxis.set_major_locator(locator)
				ax.xaxis.set_major_formatter(formatter)

			# End for loop

		return figure

	def use_seaborn_style(self):
		sns.palplot(sns.color_palette("muted"))
		sns.set_style(
			{'axes.axisbelow': True,
			 'axes.edgecolor': '.8',
			 'axes.facecolor': 'white',
			 'axes.grid': True,
			 'axes.labelcolor': '.15',
			 'axes.linewidth': 1.0,
			 'figure.facecolor': 'white',
			 'font.family': [u'sans-serif'],
			 'font.sans-serif': [u'Arial',
			  u'Liberation Sans',
			  u'Bitstream Vera Sans',
			  u'sans-serif'],
			 'grid.color': '.8',
			 'grid.linestyle': u'-',
			 'image.cmap': u'Greys',
			 'legend.frameon': True,
			 'legend.numpoints': 1,
			 'legend.scatterpoints': 1,
			 'lines.solid_capstyle': u'round',
			 'text.color': '.15',
			 'xtick.color': '.15',
			 'xtick.direction': u'out',
			 'xtick.major.size': 0.0,
			 'xtick.minor.size': 0.0,
			 'ytick.color': '.15',
			 'ytick.direction': u'out',
			 'ytick.major.size': 0.0,
			 'ytick.minor.size': 0.0}
		)
