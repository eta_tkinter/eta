from matplotlib.figure import Figure
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as NavigationToolbar
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import seaborn as sns
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import FuncFormatter
from collections import OrderedDict # for keeping options in order
import pickle # for exportin and importing parameters
import os.path # for checking if a file exist

from matplotlib.widgets import Slider, Button, RadioButtons
import numpy as np

plt.style.use('ggplot')

class Plot_Module():
    def __init__(self,guiObject):
        self.module = 'slider'
        self.gui = guiObject
        self.options = self.getParameters()

    def getOptions(self):
        settings = OrderedDict([
            ('title', 'string'),
		])
        return settings

    def getDefaults(self):
        defaults = {
            'title': None,
        }
        return defaults

    def setOptions(self, parameters):
        settings = self.getOptions()
        defaults = self.getDefaults()

        for key, value in settings.iteritems():
            if key in parameters:
                if parameters[key] != '' and parameters[key] != None:
                    if value == 'integer':
                        self.options[key] = int(float(parameters[key]))
                    else:
                        self.options[key] = parameters[key]
                else:
                    self.options[key] = defaults[key]
            else:
                self.options[key] = defaults[key]

        self.saveParameters(self.options)

    def saveParameters(self,options):
        fileName = 'parameters/plot/%s.pkl' % self.module
        with open(fileName, 'wb') as parametersFile:
            pickle.dump(options, parametersFile)

    def getParameters(self):
        fileName = 'parameters/plot/%s.pkl' % self.module
        if os.path.isfile(fileName):
            with open(fileName, 'rb') as parametersFile:
                return pickle.load(parametersFile)
        else:
            return self.getDefaults()

    def use_seaborn_style(self):
        sns.palplot(sns.color_palette("muted"))
        sns.set_style(
            {'axes.axisbelow': True,
             'axes.edgecolor': '.8',
             'axes.facecolor': 'white',
             'axes.grid': True,
             'axes.labelcolor': '.15',
             'axes.linewidth': 1.0,
             'figure.facecolor': 'white',
             'font.family': [u'sans-serif'],
             'font.sans-serif': [u'Arial',
              u'Liberation Sans',
              u'Bitstream Vera Sans',
              u'sans-serif'],
             'grid.color': '.8',
             'grid.linestyle': u'-',
             'image.cmap': u'Greys',
             'legend.frameon': False,
             'legend.numpoints': 1,
             'legend.scatterpoints': 1,
             'lines.solid_capstyle': u'round',
             'text.color': '.15',
             'xtick.color': '.15',
             'xtick.direction': u'out',
             'xtick.major.size': 0.0,
             'xtick.minor.size': 0.0,
             'ytick.color': '.15',
             'ytick.direction': u'out',
             'ytick.major.size': 0.0,
             'ytick.minor.size': 0.0}
        )

    def my_format_function(self, x, pos=None):
        x = dates.num2date(x)
        if pos == 0: # first tick
            # fmt = '%d-%m-%Y %H:%M:%S'
            fmt = '%H:%M:%S'
        else:
            fmt = '%M:%S'
        label = x.strftime(fmt)
        return label

    def update(self,val):
        print 'update'
        self.amp = self.samp.val
        self.freq = self.sfreq.val
        self.l.set_ydata(5*np.sin(2*np.pi*self.freq*self.t))
        self.fig.canvas.draw()

    def reset(self,event):
        print 'reset'
        self.sfreq.reset()
        self.samp.reset()

    def colorfunc(self,label):
        print 'colorfunc'
        self.l.set_color(label)
        self.fig.canvas.draw_idle()

    def plot(self, data):
        self.fig, self.ax = plt.subplots()
        plt.subplots_adjust(left=0.25, bottom=0.25)
        self.t = np.arange(0.0, 1.0, 0.001)
        a0 = 5
        f0 = 3
        s = a0*np.sin(2*np.pi*f0*self.t)
        self.l, = plt.plot(self.t, s, lw=2, color='red')
        plt.axis([0, 1, -10, 10])

        axcolor = 'lightgoldenrodyellow'
        self.axfreq = plt.axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)
        self.sfreq = Slider(self.axfreq, 'Freq', 0.1, 30.0, valinit=f0)
        self.sfreq.on_changed(self.update)

        self.axamp = plt.axes([0.25, 0.15, 0.65, 0.03], axisbg=axcolor)
        self.samp = Slider(self.axamp, 'Amp', 0.1, 10.0, valinit=a0)
        self.samp.on_changed(self.update)

        self.resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
        self.button = Button(self.resetax, 'Reset', color=axcolor, hovercolor='0.975')
        self.button.on_clicked(self.reset)

        self.rax = plt.axes([0.025, 0.5, 0.15, 0.15], axisbg=axcolor)
        self.radio = RadioButtons(self.rax, ('red', 'blue', 'green'), active=0)
        self.radio.on_clicked(self.colorfunc)

        canvas = FigureCanvas(self.fig)

        figure = self.fig
        height = 500

        plt.show()
        plt.close()

        # Set return variables
        returned = {
            # 'canvas': canvas,
            'figure': figure,
            'height': height,
            'slider': self.sfreq,
            'button': self.button,
        }

        return returned
