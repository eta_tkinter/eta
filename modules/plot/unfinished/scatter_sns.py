from matplotlib.figure import Figure
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as NavigationToolbar
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import seaborn as sns
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import FuncFormatter
from collections import OrderedDict # for keeping options in order
import pickle # for exportin and importing parameters
import os.path # for checking if a file exist

plt.style.use('ggplot')

class Plot_Module():
    def __init__(self,guiObject):
        self.module = 'scatter_sns' # change this to the module name
        self.gui = guiObject
        self.options = self.getParameters()

    def getOptions(self):
        settings = OrderedDict([
			('height', 'integer'),
            ('width', 'integer'),
            ('hue','maskedColumns')
		])
        return settings

    def getDefaults(self):
        defaults = {
            'height': '600',
            'width': '1000',
            'hue': None,
        }
        return defaults

    def setOptions(self, parameters):
        settings = self.getOptions()
        defaults = self.getDefaults()

        for key, value in settings.iteritems():
            if key in parameters:
                if parameters[key] != '' and parameters[key] != None:
                    if value == 'integer':
                        self.options[key] = int(float(parameters[key]))
                    else:
                        self.options[key] = parameters[key]
                else:
                    self.options[key] = defaults[key]
            else:
                self.options[key] = defaults[key]

        self.saveParameters(self.options)

    def saveParameters(self,options):
        fileName = 'parameters/plot/%s.pkl' % self.module
        with open(fileName, 'wb') as parametersFile:
            pickle.dump(options, parametersFile)

    def getParameters(self):
        fileName = 'parameters/plot/%s.pkl' % self.module
        if os.path.isfile(fileName):
            with open(fileName, 'rb') as parametersFile:
                return pickle.load(parametersFile)
        else:
            return self.getDefaults()

    def use_seaborn_style(self):
        sns.palplot(sns.color_palette("muted"))
        sns.set_style(
            {'axes.axisbelow': True,
             'axes.edgecolor': '.8',
             'axes.facecolor': 'white',
             'axes.grid': True,
             'axes.labelcolor': '.15',
             'axes.linewidth': 1.0,
             'figure.facecolor': 'white',
             'font.family': [u'sans-serif'],
             'font.sans-serif': [u'Arial',
              u'Liberation Sans',
              u'Bitstream Vera Sans',
              u'sans-serif'],
             'grid.color': '.8',
             'grid.linestyle': u'-',
             'image.cmap': u'Greys',
             'legend.frameon': False,
             'legend.numpoints': 1,
             'legend.scatterpoints': 1,
             'lines.solid_capstyle': u'round',
             'text.color': '.15',
             'xtick.color': '.15',
             'xtick.direction': u'out',
             'xtick.major.size': 0.0,
             'xtick.minor.size': 0.0,
             'ytick.color': '.15',
             'ytick.direction': u'out',
             'ytick.major.size': 0.0,
             'ytick.minor.size': 0.0}
        )

    def my_format_function(self, x, pos=None):
        x = dates.num2date(x)
        if pos == 0: # first tick
            # fmt = '%d-%m-%Y %H:%M:%S'
            fmt = '%H:%M:%S'
        else:
            fmt = '%M:%S'
        label = x.strftime(fmt)
        return label

    def plot(self, data):
        error = None

        if self.options['hue'] is None:
            error = 'No hue selected'
        else:
            hue = self.options['hue']

        sns.set()

        print data

        # df = sns.load_dataset("iris")
        # print df
        g = sns.pairplot(data, hue=hue)
        figure = g.fig

        # Set return variables
        returned = {
            'figure': figure,
            'height': self.options['height'],
            # 'width': self.options['width'],
            'error': error,
        }

        return returned
