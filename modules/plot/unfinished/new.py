from matplotlib.figure import Figure
from matplotlib.backends.backend_gtk3cairo import FigureCanvasGTK3Cairo as FigureCanvas
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as NavigationToolbar
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import seaborn as sns
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import FuncFormatter
from collections import OrderedDict # for keeping options in order
import pickle # for exportin and importing parameters
import os.path # for checking if a file exist

from matplotlib.gridspec import GridSpec # create grit for subplots
from matplotlib.patches import Rectangle # create rectangle on overview plot

plt.style.use('ggplot')

class Plot_Module():
    def __init__(self,guiObject):
        self.module = 'multiple'
        self.gui = guiObject
        self.options = self.getParameters()

    def getOptions(self):
		settings = OrderedDict([
            ('index', 'maskedColumns'),
            ('title', 'string'),
            ('legend', ['Yes','No']),
            ('height', 'integer'),
            ('width', 'integer'),
			('dpi', 'integer'),
            ('edge', ['Yes','No']),
		])
		return settings

    def getDefaults(self):
        defaults = {
            'edge': 'Yes',
            'title': None,
            'legend': 'Yes',
            'dpi': '80',
            'height': '600',
            'width': '1000',
            'index': 'time',
        }
        return defaults

    def setOptions(self, parameters):
        settings = self.getOptions()
        defaults = self.getDefaults()

        for key, value in settings.iteritems():
            if key in parameters:
                if parameters[key] != '' and parameters[key] != None:
                    if value == 'integer':
                        self.options[key] = int(float(parameters[key]))
                    else:
                        self.options[key] = parameters[key]
                else:
                    self.options[key] = defaults[key]
            else:
                self.options[key] = defaults[key]

        self.saveParameters(self.options)

    def saveParameters(self,options):
        fileName = 'parameters/plot/%s.pkl' % self.module
        with open(fileName, 'wb') as parametersFile:
            pickle.dump(options, parametersFile)

    def getParameters(self):
        fileName = 'parameters/plot/%s.pkl' % self.module
        if os.path.isfile(fileName):
            with open(fileName, 'rb') as parametersFile:
                return pickle.load(parametersFile)
        else:
            return self.getDefaults()

    def use_seaborn_style(self):
        sns.palplot(sns.color_palette("muted"))
        sns.set_style(
            {'axes.axisbelow': True,
             'axes.edgecolor': '.8',
             'axes.facecolor': 'white',
             'axes.grid': True,
             'axes.labelcolor': '.15',
             'axes.linewidth': 1.0,
             'figure.facecolor': 'white',
             'font.family': [u'sans-serif'],
             'font.sans-serif': [u'Arial',
              u'Liberation Sans',
              u'Bitstream Vera Sans',
              u'sans-serif'],
             'grid.color': '.8',
             'grid.linestyle': u'-',
             'image.cmap': u'Greys',
             'legend.frameon': True,
             'legend.numpoints': 1,
             'legend.scatterpoints': 1,
             'lines.solid_capstyle': u'round',
             'text.color': '.15',
             'xtick.color': '.15',
             'xtick.direction': u'out',
             'xtick.major.size': 0.0,
             'xtick.minor.size': 0.0,
             'ytick.color': '.15',
             'ytick.direction': u'out',
             'ytick.major.size': 0.0,
             'ytick.minor.size': 0.0}
        )

    def drawOverview(self):
        self.axfull.cla()

        # Set the automatic date locator for xticks
        locator = dates.AutoDateLocator()
        # Set the automatic date formatter for xlabels
        formatter = dates.AutoDateFormatter(locator)
        formatter.scaled[1/(24.*60.)] = '%M:%S' # only show min and sec

        j = 0
        for column in self.data:
            if column == self.index:
                pass
            else:
                self.axfull.plot_date(self.data[self.index],self.data[column],'-',color=self.color_cycler[j], label=column) # ,color=color_cycler[j]

                # Add a legend
                if self.options['legend'] == 'Yes':
                    self.axfull.legend(loc="upper right")

                if self.options['title'] != None:
                    self.axfull.set_title(self.options['title'])

                self.axfull.set_xticklabels(self.data[self.index],rotation=45)
                self.axfull.xaxis.set_major_locator(locator)
                self.axfull.xaxis.set_major_formatter(formatter)
                j += 1

            # End for loop

    def drawRectangle(self):
        self.axfull.add_patch(Rectangle((self.xlim_begin, self.ylim_begin), self.xlim_width, self.ylim_height, facecolor="grey", alpha=0.2))

    def drawZoomPlots(self):
        # Set the automatic date locator for xticks
        locator = dates.AutoDateLocator()
        # Set the automatic date formatter for xlabels
        formatter = dates.AutoDateFormatter(locator)
        formatter.scaled[1/(24.*60.)] = '%M:%S' # only show min and sec

        i = 0
        for column in self.data:
            if column != self.index:
                self.axzoom[i].cla()
                self.axzoom[i].set_xticklabels(self.data[self.index],rotation=45)
                self.axzoom[i].xaxis.set_major_locator(locator)
                self.axzoom[i].xaxis.set_major_formatter(formatter)

                self.axzoom[i].set_xlim(self.xlim_begin,self.xlim_end)
                self.axzoom[i].set_ylim(self.ylim_begin,self.ylim_end)
                self.axzoom[i].plot_date(self.data[self.index],self.data[column],'-',color=self.color_cycler[i])

                self.axzoom[i].set_xlabel(column)

                i += 1

    def setLimits(self,x,y):
        print x
        print y
        self.xlim = self.axfull.get_xlim()
        self.ylim = self.axfull.get_ylim()
        self.xlim_difference = self.xlim[1] - self.xlim[0]
        self.ylim_difference = self.ylim[1] - self.ylim[0]

        self.xlim_width = self.xlim_difference / 5
        self.ylim_height = self.ylim_difference

        self.xlim_width *= 1.1

        self.xlim_begin = self.xlim[0]
        self.xlim_end = self.xlim[0] + self.xlim_width
        self.ylim_begin = self.ylim[0]
        self.ylim_end = self.ylim[0] + self.ylim_height

    def updatePlots(self,event):
        print 'test'
        '''When mouse is right-clicked on the canvas get the coordiantes and send them to points.zoom'''
        if event.button!=1: return
        if (event.xdata is None): return
        print 'test'
        # x,y = event.xdata, event.ydata
        #
        # self.drawOverview()
        # self.setLimits(x,y)
        # self.drawRectangle()
        # self.drawZoomPlots()

    def plot(self, data):

        # Get some usefull parameters into variables
        self.index = self.options['index']
        self.data = data

        # Use the seaborn style
        self.use_seaborn_style()

        # if self.options['edge'] == 'Yes':
        #     figure = Figure(figsize=(20, 5), dpi=self.options['dpi'], edgecolor='k')
        # else:
        #     figure = Figure(figsize=(20, 5), dpi=self.options['dpi']) # Configure the matplotlib figure

        # self.figure = Figure(figsize=(20, 5), dpi=80)
        self.figure = plt.figure()
        self.figure.subplots_adjust(left=0.1, bottom=0.1,right=0.95,top=0.95,hspace=0.3)

        self.data[self.index] = pd.to_datetime(self.data[self.index])
        self.data = self.data.set_index(self.data[self.index])
        amount_columns = len(self.data.columns) - 1

        gs = GridSpec(3,amount_columns)
        self.axfull = plt.subplot2grid((3,amount_columns), (0,0), colspan=amount_columns)
        self.figure.add_axes(self.axfull)

        self.color_cycler = ['r','b','g','y']

        self.axzoom = []
        i = 0
        for column in self.data:
            if column != self.index:
                self.axzoom.append(plt.subplot2grid((3,amount_columns),(1,i), rowspan=2))
                self.figure.add_axes(self.axzoom[0])
                i += 1

        self.canvas = FigureCanvas(self.figure)

        self.drawOverview()
        self.setLimits(0,0)
        self.drawRectangle()
        self.drawZoomPlots()

        self.figure.canvas.mpl_connect('button_press_event', self.updatePlots)

        canvas = self.canvas

        # Set return variables
        returned = {
            'figure': self.figure,
            # 'canvas': canvas,
            'height': self.options['height'],
            'width': self.options['width'],
        }

        return returned
