import matplotlib
matplotlib.use('TkAgg')
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from matplotlib.ticker import FuncFormatter

import pandas as pd

from collections import OrderedDict

from numpy import random


import seaborn as sns
plt.style.use('ggplot')

class Module():
    def __init__(self):
        self.moduleName = 'zoom'

    def run(self, data, parameters):
        '''Creates random points, 2 axis on 1 figure on 1 canvas on init. Allows for drawing and zooming of points.'''
        self.n = 20
        self.xrand = random.rand(1,self.n)*10
        self.yrand = random.rand(1,self.n)*10
        self.randsize = random.rand(1,self.n)*200
        self.randcolor = random.rand(self.n,3)

        self.fig = Figure(figsize=(10,10), dpi=80)
        self.ax = self.fig.add_subplot(121)
        self.axzoom = self.fig.add_subplot(122)
        self.canvas = FigureCanvas(self.fig)

        self.draw()
        self.zoom(5,5)

        figure = self.fig
        canvas = self.canvas
        height = 500

        # self.fig.canvas.mpl_connect('motion_notify_event', self.updatecursorposition)
        self.fig.canvas.mpl_connect('button_press_event', self.updatezoom)

        return self.fig

    def draw(self):
        '''Draws the ax-subplot'''
        self.ax.cla()
        self.ax.grid(True)
        self.ax.set_xlim(0,10)
        self.ax.set_ylim(0,10)
        self.ax.scatter(self.xrand, self.yrand, marker='o', s=self.randsize, c=self.randcolor, alpha=0.5)

    def drawzoom(self):
        self.axzoom.cla()
        self.axzoom.grid(True)
        self.axzoom.set_xlim(self.x-1, self.x+1)
        self.axzoom.set_ylim(self.y-1, self.y+1)
        self.axzoom.scatter(self.xrand, self.yrand, marker='o', s=self.randsize*5, c=self.randcolor, alpha=0.5)

    def zoom(self, x, y):
        '''Adds a rectangle to the zoomed area of the ax-graph and updates the axzoom-graph'''
        self.x = x
        self.y = y
        self.draw()
        self.drawzoom()
        self.ax.add_patch(Rectangle((x - 1, y - 1), 2, 2, facecolor="grey", alpha=0.2))
        self.fig.canvas.draw()

    def updatezoom(self,event):
        '''When mouse is right-clicked on the canvas get the coordiantes and send them to points.zoom'''
        if event.button!=1: return
        if (event.xdata is None): return
        x,y = event.xdata, event.ydata
        self.zoom(x,y)

    # def updatecursorposition(self,event):
    #     '''When cursor inside plot, get position and print to statusbar'''
    #     if event.inaxes:
    #         x = event.xdata
    #         y = event.ydata
    #         self.gui.statbar.push(1, ("Coordinates:" + " x= " + str(round(x,3)) + "  y= " + str(round(y,3))))
