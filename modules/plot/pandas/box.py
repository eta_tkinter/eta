import matplotlib
matplotlib.use('TkAgg')
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

import pandas as pd
import seaborn as sns

class Module():
    def __init__(self):
        self.moduleName = 'box' # change this to the module name

    def getParameters(self):
        from collections import OrderedDict
        settings = OrderedDict([
            ('subplots', 'boolean'),
            ('title', 'string'),
            ('dpi', 'integer'),
            ('figsize_height', 'integer'),
            ('figsize_width', 'integer'),
        ])
        return settings

    def getDefaults(self):
        defaults = {
            'subplots': True,
            'title': None,
            'dpi': 80,
            'figsize_height': 4,
            'figsize_width': 10,
        }
        return defaults

    # http://pandas.pydata.org/pandas-docs/version/0.18.1/generated/pandas.DataFrame.plot.html#pandas.DataFrame.plot
    def run(self, data, parameters):
        self.use_seaborn_style()

        subplots = parameters['subplots']
        title = parameters['title']
        figsize = (parameters['figsize_width'], parameters['figsize_height'])
        dpi = parameters['dpi']

        fig = plt.figure(figsize=figsize, dpi=dpi)
        ax = fig.add_subplot(111)
        lines = data.plot(kind='box', subplots=subplots, title=title, ax=ax, stacked=True)

        return fig

    def use_seaborn_style(self):
        sns.palplot(sns.color_palette("muted"))
        sns.set_style(
            {'axes.axisbelow': True,
             'axes.edgecolor': '.8',
             'axes.facecolor': 'white',
             'axes.grid': True,
             'axes.labelcolor': '.15',
             'axes.linewidth': 1.0,
             'figure.facecolor': 'white',
             'font.family': [u'sans-serif'],
             'font.sans-serif': [u'Arial',
              u'Liberation Sans',
              u'Bitstream Vera Sans',
              u'sans-serif'],
             'grid.color': '.8',
             'grid.linestyle': u'-',
             'image.cmap': u'Greys',
             'legend.frameon': False,
             'legend.numpoints': 1,
             'legend.scatterpoints': 1,
             'lines.solid_capstyle': u'round',
             'text.color': '.15',
             'xtick.color': '.15',
             'xtick.direction': u'out',
             'xtick.major.size': 0.0,
             'xtick.minor.size': 0.0,
             'ytick.color': '.15',
             'ytick.direction': u'out',
             'ytick.major.size': 0.0,
             'ytick.minor.size': 0.0}
        )
