import subprocess
import pandas as pd
import missingno as msno
import sys,webbrowser,os
import pandas_profiling

class Module():
	def __init__(self):
		self.moduleName = 'pandas_profiling'

	# def getParameters(self):
	# 	parameters = {
	# 		'Output rejected variables': ['Yes', 'No']
	# 	}
	# 	return parameters

	def run(self,data,parameters):
		# response = subprocess.Popen(self.profile(data,parameters),shell=True)
		self.profile(data,parameters)
		message = ["Running pandas-profiler", "Pandas-profiling is running in the background. This may take a few minutes. When finished it is opened automatically."]
		return message

	def profile(self,data,parameters):
		df = data

		# if len(df.index) < 7500:
		# 	sample = df.sample(len(df.index))
		# else: #to maintain speed
		# 	sample = df.sample(7500)

		sample = df.sample(len(df.index))

		profile = pandas_profiling.ProfileReport(sample)

		# if parameters['Output rejected variables'] == 'Yes':
		# 	rejected_variables = profile.get_rejected_variables(threshold=0.9)
		# 	print(rejected_variables)

		path = "./tmp/myoutputfile.html"
		profile.to_file(outputfile=path)

		try:
			webbrowser.open(path)
		except BaseException:
			print ("pandas-profiling failed")
		else: # Mac OS
			os.system("open /Applications/Safari.app %s" % path)

		# gui.clearStatusbar()
