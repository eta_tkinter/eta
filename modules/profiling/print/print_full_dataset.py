import pandas as pd

class Module():
    def __init__(self):
        self.moduleName = 'print_full_dataset'

    def print_full(self, message):
        pd.set_option('display.max_rows', len(message))
        print(message)
        pd.reset_option('display.max_rows')

    def run(self,data,parameters):
        self.print_full(data)
