import pandas as pd
import seaborn as sns

class Module():
    def __init__(self):
        self.moduleName = 'describe'

    def getParameters(self):
        from collections import OrderedDict
        settings = OrderedDict([
            ('correlation_method', ['pearson','kendall','spearman']),
        ])
        return settings

    def getDefaults(self):
        defaults = {
            'correlation_method': 'pearson',
        }
        return defaults

    def print_full(self, to_print):
        pd.set_option('display.max_rows', len(to_print))
        print(to_print, "\n")
        pd.reset_option('display.max_rows')

    def run(self,data,parameters):
        print("CORRELATION:")
        corr = data.corr(method=parameters['correlation_method'])
        self.print_full(corr)
