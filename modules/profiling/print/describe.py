import pandas as pd

class Module():
    def __init__(self):
        self.moduleName = 'describe'

    def print_full(self, to_print):
        pd.set_option('display.max_rows', len(to_print))
        pd.set_option('display.width', 1000)
        pd.set_option('display.max_columns', 500)
        print(to_print, "\n")
        pd.reset_option('display.max_rows')

    def run(self,data,parameters):
        print("DESCRIBE:")
        describe = data.describe(include='all')
        self.print_full(describe)
