from collections import OrderedDict # for keeping options in order

# import your own packages here

class Module():
	def __init__(self):
		self.moduleName = 'name' #TODO: rename

    def getParameters(self):
        settings = OrderedDict([
            # ('KEY', 'OPTION'), THE DICT STRUCTURE
            ('column1', 'columns'), # a column selection from selected columns
            ('str', 'string'), # a string textbox
            ('list', ['Option1','Option2','Option3']), # a dropdown menu of list items
			('int', 'integer'), # a textbox that gets converted to an integer
            ('flt', 'float'), # a textbox that gets converted to a float
		])
        return settings

	def getDefaults(self):
        defaults = {
            # Put default values of the settings here
            'column1': None,
            'str': 'String',
            'list': 'Option1',
            'int': '100',
            'flt': '200.20',
        }
		return defaults

	def run(self,data, parameters):
		column = parameters['column1'] # Get your parameters

		# put your code here

		# if plot:
		return figure # return matplotlib figure

		# if preprocesing:
		# a dict that is returned to the handler
		returned = {
			'newDataFrame': True # True or False, whether to create a new data frame or add to the current data frame
			'data': data, # the data that will be added
			'name_addition': self.moduleName # the name that will be appended to the data frame or data columns
		}
		return returned # return above dict
