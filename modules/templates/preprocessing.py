from collections import OrderedDict # for keeping options in order
import pickle # for exporting and importing parameters
import os.path # for exporting and importing parameters

# import your own packages here

class Module():
	def __init__(self):
		self.moduleName = 'name' #TODO: rename
		self.options = self.getParameters()

    def getOptions(self):
        settings = OrderedDict([
            # ('KEY', 'OPTION'), THE DICT STRUCTURE
            ('column1', 'columns'), # a column selection from all columns
            ('column2', 'maskedColumns'), # a column selection from the selected columns
            ('str', 'string'), # a string textbox
            ('list', ['Option1','Option2','Option3']), # a dropdown menu of list items
			('int', 'integer'), # a textbox that gets converted to an integer
            ('flt', 'float'), # a textbox that gets converted to a float
		])
        return settings

	def getDefaults(self):
        defaults = {
            # Put default values of the settings here
            # 'column1': None,
            # 'column2': 'time',
            # 'str': 'String',
            # 'list': 'Column',
            # 'int': '100',
            # 'flt': '200.20',
        }
		return defaults

	def setOptions(self, parameters):
		settings = self.getOptions()
		defaults = self.getDefaults()

		for key, value in settings.iteritems():
			if key in parameters:
				if parameters[key] != '' and parameters[key] != None:
					if value == 'integer':
						self.options[key] = int(float(parameters[key]))
					elif value == 'float':
						self.options[key] = float(parameters[key])
					else:
						self.options[key] = parameters[key]
				else:
					self.options[key] = defaults[key]
			else:
				self.options[key] = defaults[key]

		self.saveParameters(self.options)

	def saveParameters(self,options):
		fileName = 'parameters/preprocessing/%s.pkl' % self.moduleName
		with open(fileName, 'wb') as parametersFile:
			pickle.dump(options, parametersFile)

	def getParameters(self):
		fileName = 'parameters/preprocessing/%s.pkl' % self.moduleName
		if os.path.isfile(fileName):
			with open(fileName, 'rb') as parametersFile:
				return pickle.load(parametersFile)
		else:
			return self.getDefaults()

	def run(self,dataFrames,parameters=None,):

		# put your code here

		returned = {
			'newDataFrame': True, # return True or False
			'data': resampledData, # return the data (or None if no data needs to be created)
			'name_addition': timestamp_field + resample_field # the name
		}

		return returned
        # RETURN DATA, MESSAGE , IF WHOLE DATASET NEEDS TO BE REPLACED
